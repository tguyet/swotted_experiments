#!/bin/bash
exp="EXP_$(date +%F)_$(date +%s)"

mkdir -p "$exp/"
file="$exp/results.csv"

echo -e "it,R_hidden,Tw_hidden,K,N,T,R,Tw,loss,model,normalization,sparsity,pheno_nonsuccession,error_Ph,error_W_train,error_X_train,time,error_W_test,error_X_test" >> $file

# default synthetic dataset parameters
K=200
N=20
T=10
Rhidden=4
Twhidden=1 # 1 is mandatory for comparison with LogPar, CNTF and SWIFT in order to be able to compute the FIT

# default models' parameters
R=4
Tw=1
loss='Bernoulli'
normalization='True'
phenotypesuccession=0.25
sparsity=0.25
batchsize=50
epochs=100

for R in 12 36
do
Rhidden=$R
for Tw in 1 3 5
do
Twhidden=$Tw
for N in 20
do
for T in 50
do
for it in {1..10}
do  
    dataset="$exp/data_${it}_sw.pickle"
    python3 experiments_gen_data.py -k $K -n $N -t $T -r $Rhidden -tw $Twhidden -tr -sw -p $dataset
    
    for model in fastSWoTTeD SWoTTeD #CNTF LogPar SWIFT
    do 
        if [ "$model" = "SWoTTeD" ]; then
            echo "running SWoTTeD..." 
            cmd="../competitors/run_swotted.py -it $it -l $loss -p $dataset -r $R -tw $Tw -b $batchsize -e $epochs -sp $sparsity -ps $phenotypesuccession"
            if [ "$normalization" = "True" ]; then
                cmd="${cmd} -nr"
            fi
            res=$(python3 $cmd)
        elif [ "$model" = "fastSWoTTeD" ]; then
            echo "running fastSWoTTeD..." 
            cmd="../competitors/run_fastswotted.py -it $it -l $loss -p $dataset -r $R -tw $Tw -b $batchsize -e $epochs -sp $sparsity -ps $phenotypesuccession"
            if [ "$normalization" = "True" ]; then
                cmd="${cmd} -nr"
            fi
            res=$(python3 $cmd)
        elif [ "$model" = "CNTF" ]; then
            echo "running CNTF..." 
            res=$(python3 ../competitors/cntf/run_cntf.py -it $it -p $dataset -r $R) 
        elif [ "$model" = "SWIFT" ]; then 
            echo "running SWIFT..." 
            res=$(python3 ../competitors/swift/run_swift.py -it $it -p $dataset -r $R)
        elif  [ "$model" = "LogPar" ]; then
            echo "running LogPar..." 
            res=$(python3 ../competitors/LogPar/run_logpar.py -it $it --data_path $dataset --rank $R --epochs 500 --batch_size 50 --name $exp) 
        fi

        echo -e "$it,$Rhidden,$Twhidden,$K,$N,$T,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession, $res" >> $file
    done

done
done
done
done
done

