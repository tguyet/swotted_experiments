"""


@author: Hana Sebia, Inria
"""

## required to simply execute from the experiments repository
import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import argparse
import pickle
import time
import torch
import numpy as np
from gen_data import AddNoise_deleteEvents, AddNoise_addEvents, AddNoise_addEvents_Poisson


def get_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument("-p", "--datasetfile",
			help="specify the filename of input data")
	parser.add_argument("-pro","--probability", type=float,
			help="specify the probability of adding/deleting an event")
	parser.add_argument("-ae","--add_events", action="store_true")
	parser.add_argument("-de","--delete_events", action="store_true")

	
	return parser.parse_args()



if __name__ == '__main__':

	args = get_arguments()

	# Load synthetic data (containing train and test)
	#try:
	Ph_, W_train, X_train, W_test, X_test, params = pickle.load(open(args.datasetfile, "rb"))
	#except ValueError:
	#	print("you probably use a 'classical' dataset ... this script need a synthetic dataset with train/test to work")
	#	exit(0)

	nb_of_events = np.sum([torch.sum(X) for X in X_train])
	if args.delete_events:
		noisy_dataset, nb_of_noisy_events = AddNoise_deleteEvents(dataset=X_train, proba=args.probability)
	elif args.add_events:
		noisy_dataset, nb_of_noisy_events = AddNoise_addEvents_Poisson(X_train, args.probability)
	
	pickle.dump((torch.cat((W_train,W_test)), Ph_, noisy_dataset+X_test, params), open(args.datasetfile+".noisy", "wb"))
	print(str(nb_of_noisy_events) + "," + str(nb_of_events))

