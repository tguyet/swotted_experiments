import os
import argparse
import pickle

from gen_data import gen_synthetic_data, gen_phenosuccession_synthetic_data
from sklearn.model_selection import train_test_split


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-k",
        "--patients",
        type=int,
        help="specify the number of patients (default 100)",
        default=100,
    )
    parser.add_argument(
        "-n",
        "--medical_events",
        type=int,
        help="specify the number of medical events (default 20)",
        default=20,
    )
    parser.add_argument(
        "-t",
        "--time",
        type=int,
        help="specify the length of the patients stay (default 8)",
        default=8,
    )
    parser.add_argument(
        "-r",
        "--phenotypes",
        type=int,
        help="specify the number of phenotypes (default 5)",
        default=5,
    )
    parser.add_argument(
        "-tw",
        "--temporal_window",
        type=int,
        help="specify the length of the temporal window (default 3)",
        default=3,
    )
    parser.add_argument(
        "-sw",
        "--sliding_window",
        action="store_true",
        help="generating patient matrices with sliding windows (default T)",
        default=True,
    )
    parser.add_argument(
        "-no", "--noise", type=float, help="add noise (default False)", default=0.0
    )
    parser.add_argument(
        "-tr",
        "--truncate",
        action="store_true",
        help="truncate values greater than 1 (default True)",
        default=True,
    )
    parser.add_argument(
        "-p",
        "--path",
        help="specify the path to store the generated data",
        default="./data.pickle",
    )
    parser.add_argument(
        "-tt",
        "--test",
        default=0.0,
        type=float,
        help="if not nul, it generates a train/test dataset, the specified value (in) indicates the proportion for the test data",
    )
    parser.add_argument(
        "-ph",
        "--pheno",
        action="store_true",
        default=False,
        help="use specific pheno-non-succession dataset (ignore many parameters)",
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = get_arguments()

    if args.pheno:
        W_, Ph_, X, params = gen_phenosuccession_synthetic_data(
            args.patients,
            args.time,
            truncate=args.truncate,
            eventdensity=0.3,
            nooverlap=True,
        )

    else:
        W_, Ph_, X, params = gen_synthetic_data(
            args.patients,
            args.medical_events,
            args.time,
            args.phenotypes,
            args.temporal_window,
            sliding_window=args.sliding_window,
            noise=args.noise,
            truncate=args.truncate,
            phenodensity=0.3,
        )

    if not os.path.exists(os.path.dirname(args.path)):
        os.makedirs(os.path.dirname(args.path))

    if args.test <= 0.0 or args.test > 1.0:
        pickle.dump((W_, Ph_, X, params), open(args.path, "wb"))
    else:
        # Split the data into train/test dataset
        X_train, X_test, W_train, W_test = train_test_split(
            X, W_, train_size=1 - args.test, test_size=args.test
        )
        pickle.dump(
            (Ph_, W_train, X_train, W_test, X_test, params), open(args.path, "wb")
        )
