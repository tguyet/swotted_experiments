#!/bin/bash

exp="EXP_BIKE_$(date +%F)_$(date +%s)"

mkdir -p "$exp/"
file="$exp/results.csv"

echo -e "it,R,Tw,loss,model,normalization,sparsity,pheno_succession,error_Ph,error_W_train,error_X_train,time,error_W_test,error_X_test" >> $file

#### default parameters
R=12

loss='Bernoulli'
normalization='True'
phenotypesuccession=0.25
sparsity=0.25
batchsize=50
epochs=400

dataset="../data/bike_size6.pkl"
#dataset_logpar="../data/mimic_data_logpar.pkl"

for R in 36
do

for it in {1..4}
do
    for Tw in 1 2 3 4 # 5 4 3 2 1
    do
        for model in "fastswotted" "swotted"
        do
            cmd="../competitors/run_$model.py -it $it -l $loss -p $dataset -r $R -tw $Tw -b $batchsize -e $epochs -sp $sparsity -ps $phenotypesuccession -mi -dir $exp"
            if [ "$normalization" = "True" ]; then
                cmd="${cmd} -nr"
            fi
            res=$(python3 $cmd)
            echo -e "$it,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession, $res" >> $file
        done
    done
    for model in CNTF #LogPar CNTF SWIFT
    do 
        Tw=1
        if [ "$model" = "CNTF" ]; then
            echo "running CNTF..." 
            res=$(python3 ../competitors/cntf/run_cntf.py -it $it -e $epochs -p $dataset -r $R -dir $exp)
            #mv $exp/model.pkl $exp/model_${model}_${Tw}_${it}.pkl 
        elif [ "$model" = "SWIFT" ]; then
            echo "running SWIFT ..." 
            res=$(python3 ../competitors/swift/run_swift.py -it $it -e 100 -p $dataset -r $R --name $exp)
        elif  [ "$model" = "LogPar" ]; then
            echo "running LogPar ..." 
           res=$(python3 ../competitors/LogPar/run_logpar.py -it $it --data_path $dataset_logpar --rank $R --epoch 100 --lr 0.001 --name $exp -m)
        fi
        
        echo -e "$it,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession, $res" >> $file
    done
done
done
