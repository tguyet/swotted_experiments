#!/bin/bash

exp="EXP_MIMIC_$(date +%F)_$(date +%s)"

mkdir -p "$exp/"
file="$exp/results.csv"

cp $0 $exp

echo -e "it,R,Tw,loss,model,normalization,sparsity,pheno_succession,error_Ph,error_W_train,error_X_train,time,error_W_test,error_X_test " >> $file


#### default parameters
R=12

loss='Bernoulli'
normalization='True'
phenotypesuccession=0.25
sparsity=0.25
batchsize=50
epochs=300

dataset="../data/mimic_data_tuple_size10.pkl"

for R in 12 36
do
for it in {1..5}
do  
	for Tw in 5 3 1
	do
        echo "running SWoTTeD with Tw=$Tw ..." 
		model="SWoTTeD"
        cmd="../competitors/run_swotted.py -it $it -l $loss -p $dataset -r $R -tw $Tw -b $batchsize -e $epochs -sp $sparsity -ps $phenotypesuccession -mi"
        if [ "$normalization" = "True" ]; then
            cmd="${cmd} -nr"
        fi
        res=$(python3 $cmd)
        echo -e "$it,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession, $res" >> $file
        
        echo "running FastSWoTTeD with Tw=$Tw ..." 
		model="FastSWoTTeD"
        cmd="../competitors/run_fastswotted.py -it $it -l $loss -p $dataset -r $R -tw $Tw -b $batchsize -e $epochs -sp $sparsity -ps $phenotypesuccession -mi"
        if [ "$normalization" = "True" ]; then
            cmd="${cmd} -nr"
        fi
        res=$(python3 $cmd)
        echo -e "$it,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession, $res" >> $file
	done
done
done
