"""
Module of synthetic 3D tensor generation mimicking collections of care pathways. 
The generated tensors are assumed to have three dimensions: idpatient, medical features and time.

@author: Mathieu Chambard, ENS Rennes
@author: Hana Sebia, Inria
@author: Thomas Guyet, Inria

@date Mar 23 2022
"""

from math import trunc
from re import sub
import numpy as np
import random
from pandas import concat
import torch
from torch.nn.functional import normalize
from munkres import Munkres
import json
from datetime import datetime

from numpy.random import Generator


def genPartition(N, R):
    l = [k for k in range(N)]
    random.shuffle(l)
    sortie = []
    restant = R
    for _ in range(R - 1):
        N = len(l)
        nombre = random.randint(
            N // restant - N // (3 * restant), N // restant + N // (3 * restant)
        )
        sortie.append(l[:nombre])
        l = l[nombre:]
        restant -= 1
    sortie.append(l)
    return sortie


def ajouteIntersection(partition, N, R):
    l = [k for k in range(N)]
    for k in range(R):
        random.shuffle(l)
        nombre = random.randint(0, N // (2 * R))
        partition[k] += l[:nombre]
    return partition


def genV(N, R, start=0.5):
    """
    Params :
    - N : number of medical events (drugs, procedures,..)
    - R : number of phenotypes
    Returns :
    - V : phenotype matrix of size R x N
    """
    V = np.zeros((R, N), dtype=float)
    part = genPartition(N, R)
    print(part)
    part = ajouteIntersection(part, N, R)
    print(part)
    for i in range(R):
        for j in part[i]:
            V[i][j] = random.uniform(start, 1)
    return V


def genUk(R, Ik, nmin=0, nmax=2):
    """
    Params :
    - R : number of phenotypes
    - Ik : length of patient's stay
    Returns :
    - Uk : patient matrix of size Ik (random) x R, where Ik denotes the length of patient's stay of patient k
    """
    Uk = np.zeros((Ik, R), dtype=float)
    for i in range(Ik):
        nombre = random.randint(nmin, nmax)
        for _ in range(nombre):
            l = random.randint(0, R - 1)
            Uk[i][l] = random.uniform(0.5, 1)
    return Uk


def genU(R, k):
    """
    Params :
    - R : number of phenotypes
    - k : number of patients
    Returns :
    - U : K patients matrices of size Ik (random) x R, where Ik denotes the length of patient's stay of patient k
    """
    tensor = []
    for _ in range(k):
        tensor.append(genUk(R, random.randint(5, 5)))
    return np.asarray(tensor)


def genData(N, M, R, K):
    """
    Params :
    - N : number of procedures
    - M : number of drugs
    - R : number of phenotypes
    - k : number of patients
    Returns :
    - V : phenotype matrix of size R x N+M
    - U : K patients matrices of size Ik (random) x R, where Ik denotes the length of patient's stay of patient k
    """
    V = genV(N + M, R)
    U = genU(R, K)
    return U, V


def genTensor3D(N, M, R, K):
    """
    Params :
    - N : number of procedures
    - M : number of drugs
    - R : number of phenotypes
    - K : number of patients
    Returns :
    - tensor3D : K matrices of size Ik x N+M, where Ik is drawn randomly and represents the the length of patient's stay
    """
    U, V = genData(N, M, R, K)  # generate patients pathways description and phenotypes
    U_ = torch.from_numpy(U)
    V_ = torch.from_numpy(V)
    tensor3D = torch.tensordot(U_, V_, dims=1)
    return U, V, tensor3D


def vector_to_matrix_phenotype(pheno, N, M):
    """
    Params :
    - pheno : matrix of size 1 x (N+M), a phenotype is a linear combination of medical events
    - N : number of procedures
    - M : number of drugs
    Returns :
    - pheno_matrix : a matrix of size N x M
    """
    procs = pheno[:N]
    drugs = pheno[N:]
    pheno_matrix = np.dot(np.transpose(procs), drugs)
    return pheno_matrix


def genTensor4D(N, M, R, K):
    """
    Params :
    - N : number of procedures
    - M : number of drugs
    - R : number of phenotypes
    - K : number of patients
    Returns :
    - tensor4D : K 3D tensor of size Ik x N x M, where Ik is drawn randomly and represents the the length of patient's stay
    """
    # generate patients pathways description and phenotypes
    U, V = genData(N, M, R, K)  # U of size : K x Ik x R and V od size : R x (N+M)

    # Change phenotypes representation into matrices of size R x N x M
    V_ = np.zeros((R, N, M), dtype=float)
    for r in range(R):
        V_[r] = vector_to_matrix_phenotype(V[r], N, M)

    # Build the 4D tensor
    U = torch.from_numpy(U)
    V = torch.from_numpy(V_)
    tensor4D = torch.tensordot(U, V, dims=1)
    return tensor4D


def binarize(x):
    """
    Params :
    - x : table
    Returns :
    - boolean indicating if the sum of table's elements >=1
    """
    return int(x.sum() >= 1)


def countMedication(patient):
    """
    Params :
    - patient : 3D tensor, Ik matrices of size N x M where Ik the length of patient's stay, N nb of procedures and M nb of drugs
    Returns :
    - table of size N, indicating the number of medications used for each procedure
    """
    # tab est de taille Ik,N,M
    return np.sum(np.apply_along_axis(binarize, 2, patient), axis=0)


def genVkWithTemporalWindow(N, R, Tstep):
    """
    Params :
    - N : number of medical events
    - R : number of phenotypes
    - Tstep : length of time's window
    Returns :
    - Vk : R matrices of phenotypes of size : N x T, or a matrice of size R x N
    """
    Vk = torch.zeros((R, N, Tstep))
    """for k in range(R):
        for t in range(Tstep):
            m = random.randint(1, N) # draw a number of medical events
            med = random.sample([i for i in range(N)], m) # select a group of medical events
            for l in med:
                Vk[k][l][t] = 1 #random.uniform(0,1)"""

    """for k in range(R):
        for l in range(N):
            same = True

            while same:
                m = random.randint(1, int(Tstep/2)) # draw a number of medical events
                med = random.sample([i for i in range(Tstep)], m) # select a group of medical events

                res = []
                for i in range(k):
                    if Vk[i][l] == med:
                        res.append(i)
                        break
                
                if len(res) == 0:
                    same = False

            
            for t in med:
                Vk[k][l][t] = 1 #random.uniform(0,1)"""

    for k in range(R):
        m = random.randint(1, int(N / 4))  # draw a number of medical events
        med = random.sample(
            [i for i in range(N)], m
        )  # select a group of medical events

        for l in med:
            if Tstep == 1:
                Vk[k][l][0] = 1
            else:
                t = random.randint(1, int(Tstep / 2))  # draw a number of days
                days = random.sample(
                    [i for i in range(Tstep)], t
                )  # select a group of days

                for d in days:
                    Vk[k][l][d] = 1

    return Vk


def genUkWithTemporalWidow(K, T, R):
    """
    Params :
    - K : number of patients
    - T : time (length of time's stay / window)
    - R : number of phenotype
    Returns :
    - Uk : K matrices of patients pathways of size : T x R
    """
    Uk = torch.zeros((K, T, R))
    for k in range(K):
        for t in range(T):
            nbPh = random.randint(1, R)  # draw a number of phenotypes
            lph = random.sample(
                [i for i in range(R)], nbPh
            )  # select a group of nbPh phenotypes
            for p in lph:
                Uk[k][t][p] = 1  # random.uniform(0,1)
    return Uk


def genTensor4DWithTemporalWindow(K, N, T, R, Tstep, noise=0.0, truncate=True):
    """
    Params :
    - K : number of patients
    - N : number of medical events
    - T : length of time's stay
    - R : number of phenotype
    - Tstep : length of time's window
    - noise : float
    Returns :
    - tensor4D : K 3D tensors of size (T/Tstep) x N x step
    """
    Uk = genUkWithTemporalWidow(K, T // Tstep, R)  # generate patients pathways
    Vk = genVkWithTemporalWindow(N, R, Tstep)  # generate temporal phenotypes

    tensor4D = torch.tensordot(Uk, Vk, dims=1)
    if truncate:
        tensor4D.data[tensor4D.data > 1] = 1  # truncate values greater than n

    if noise > 0:
        Uk += noise * np.random.normal(size=Uk.shape)  # ajout d'un bruit

    return Uk, Vk, tensor4D


def reorderPhenotypes(gen_pheno, resulting_pheno, pathways):
    """
    Params :
    - gen_pheno : a 3D tensor containing matrices of generated phenotypes
    - resulting_pheno : a 3D tensor containing matrices of resulting phenotypes
    - pathways : a 3D tensor representing patients pathways
    Returns :
    - gen_pheno : a 3D tensor containing matrices of generated phenotypes
    - reordered_pheno : a 3D tensor containing matrices of reordered resulting phenotypes
    - pathways : a 3D tensor representing patients reordered pathways
    """
    dic = np.zeros((resulting_pheno.shape[0], gen_pheno.shape[0]))

    for i in range(resulting_pheno.shape[0]):
        for j in range(resulting_pheno.shape[0]):
            dic[i][j] = torch.norm((gen_pheno[i] - resulting_pheno[j]), p="fro").item()

    m = Munkres()  # Use of Hungarian Algorithm to find phenotypes correspondances
    indexes = m.compute(dic)

    # Reorder phenotypes
    reordered_pheno = torch.zeros((resulting_pheno.shape))
    for row, column in indexes:
        reordered_pheno[row] = resulting_pheno[column]

    # Reorder pathways
    reordered_pathways = torch.zeros((pathways.shape))
    for i in range(pathways.shape[0]):
        for row, column in indexes:
            for j in range(pathways.shape[1]):
                reordered_pathways[i][j][row] = pathways[i][j][column]

    return gen_pheno, reordered_pheno, reordered_pathways


def addNoise4DTensor_ModifyEvents(dataset, nb_events, rand, remove):
    """
    Params :
    - data : 4Dtmux tensor containing patients information
    - nb_events : the number of deleted events
    - rand : boolean, if true draw a random number of patients
    - remove : boolean, if true remove events else delete events
    Returns :
    - X : disturbed 4D tensor
    """
    K = dataset.shape[0]  # number of patients
    N = dataset.shape[2]  # number of medical events
    Tstep = dataset.shape[3]  # length of time's window
    T = dataset.shape[1] * Tstep  # length of time's stay

    modified_dataset = dataset.clone()

    if rand:
        nb_patients = random.randint(0, K - 1)  # draw a number of patients
        print(nb_patients)
        patients = random.sample(
            [i for i in range(K)], nb_patients
        )  # select a group of nb_patients patients
    else:
        patients = [i for i in range(K)]

    for i in patients:
        j = 0
        while j < nb_events:
            m = random.randint(0, N - 1)  # draw a medical event to delete
            t = random.randint(0, T - 1)  # draw an instant
            if modified_dataset[i][t // Tstep][m][t % Tstep] > 0 and remove:
                modified_dataset[i][t // Tstep][m][
                    t % Tstep
                ] -= 1  # delete the medical event

            elif modified_dataset[i][t // Tstep][m][t % Tstep] == 0 and not remove:
                modified_dataset[i][t // Tstep][m][
                    t % Tstep
                ] += 1  # add the medical event
            j = j + 1

    return modified_dataset


def genDoc_fromData_forHdlsm(tensor):
    """
    Params :
    - tensor : a 4D tensor representing patients
    - exp : the name of the directory where to stock the data
    """
    # concatenate the patient's temporal slices into a single matrix
    patients = [tensor[i] for i in range(tensor.shape[0])]
    list_patients = []
    for p in range(len(patients)):
        patients[p] = [patients[p][j] for j in range(patients[p].shape[0])]
        list_patients.append(torch.cat(patients[p], 1))

    # build a list of dictionnaries for each patient
    for p in range(len(list_patients)):
        list_dic = []
        for w in range(list_patients[p].shape[0]):
            for t in range(list_patients[p].shape[1]):
                if list_patients[p][w][t] > 0:
                    list_dic.append({"w": w, "t": t})
        if len(list_dic) > 0:
            with open(
                "/home/hana/Documents/HDLSM/rtavenard/sample_data/" + str(p) + ".json",
                "w",
            ) as fout:
                json.dump(list_dic, fout)


def from_tensor_to_hdlsm(tensor, repository="sample_data"):
    """
    Params:
    - tensor: a list of tensor of size N x T, where N is the number of medical events and T is the temporal dimension
    """

    # build a list of dictionnaries for each patient
    i = 0
    for p in range(len(tensor)):
        list_dic = []
        for w in range(tensor[p].shape[0]):
            for t in range(tensor[p].shape[1]):
                if tensor[p][w][t] > 0:
                    list_dic.append({"w": w, "t": t})

        if len(list_dic) > 0:
            with open(
                "/home/hana/Documents/temporal_phenotyping/Code/temp_phenotyping/competitors/hdlsm/"
                + repository
                + "/"
                + str(i)
                + ".json",
                "w",
            ) as fout:
                json.dump(list_dic, fout)
            i = i + 1


def genUkWithSlidingTemporalWidow(K, T, R, Tw, eventdensity=0.5, nooverlap=True):
    """
    Params :
    - K : number of patients
    - T : time (length of time's stay / window)
    - R : number of phenotype
    - Tw : length of the phenotype's temporal window
    - eventdensity :
    Returns :
    - Uk : K matrices of patients pathways of size : R x (T - Tw + 1)
    """
    Uk = torch.zeros((K, R, (T - Tw + 1)))
    for k in range(K):
        for r in range(R):
            for t in range((T - Tw + 1)):
                choice = random.uniform(0, 1)

                # no overlap
                i = 0
                while nooverlap and choice < 1 and i < Tw:
                    if t - i >= 0:
                        if Uk[k][r][t - i] == 1:
                            choice = 1
                    if t + i < T - Tw:
                        if Uk[k][r][t + i] == 1:
                            choice = 1
                    i = i + 1
                Uk[k][r][t] = choice <= eventdensity

    return Uk


def genTensor3DWithSlidingTemporalWindow(K, N, T, R, Tw, noise=0.0, truncate=True, phenodensity=0.5):
    """
    Params :
    - K : number of patients
    - N : number of medical events
    - T : length of time's stay
    - R : number of phenotype
    - Tw : length of time's window
    - noise : float
    Returns :
    - tensor3D : K matrices of size  N x T
    """
    Uk = genUkWithSlidingTemporalWidow(K, T, R, Tw, eventdensity=phenodensity)  # generate patients pathways
    Vk = genVkWithTemporalWindow(N, R, Tw)  # generate temporal phenotypes

    if noise > 0:
        Uk += noise * np.random.normal(size=Uk.shape)  # ajout d'un bruit

    Y = []
    for p in range(K):
        # create a tensor of windows
        dec = torch.tensordot(Uk[p], Vk, dims=([0], [0]))

        # now ... windows have to be summed
        Yp = torch.zeros((N, T))
        for i in range(T - Tw + 1):
            Yp[:, i : (i + Tw)] += dec[i]

        Y.append(Yp)

    Y = torch.stack(Y)
    if truncate:
        Y.data[Y > 1] = 1  # truncate values greater than 1

    return Uk, Vk, Y


def gen_phenosuccession_synthetic_data(
    K=100, T=6, truncate=True, eventdensity=0.5, nooverlap=False, **_kwargs
):
    """
    This function generates a datasets of size `K times 20`, ie 20 features.
    It hides R=10 patterns. 6 of these patterns are random, but 6 are designed
    manually to contain event redundancies in time.

    Params
    - K: number of patients
    - T: length of time's stay (must be larger than 3)
    - sliding_window: bool, if true genUkwithSlidingTemporalWindow
    - noise: float, if >0 add a normal noise to the generated tensor
    Returns
    - W_  third-order tensor representing pathways
    - Ph_: third-order tensor prepresenting phenotyes or a matrice of size R x N
    - X: a list of K matrices of size  N x T
    - params: a dictionary containing the parameters used to generate the dataset
    """
    N = 20
    Tw_hidden = 3
    R_hidden = 10

    params = {}
    params[
        "source"
    ] = "synthetic"  # this indicates the source of the dataset (its name, version of the generator, etc.)
    # params["date"] = datetime.now()  # keep the date at which it has been generated
    # matrix dimensions
    params["K"] = K
    params["N"] = N
    params["T"] = T
    # synthetic data parameters
    params["sliding_window"] = True
    params["R_hidden"] = R_hidden
    params["Tw_hidden"] = Tw_hidden
    params["noise"] = 0.0
    params["truncate"] = truncate
    params["eventdensity"] = eventdensity

    W_ = genUkWithSlidingTemporalWidow(
        K, T, R_hidden, Tw_hidden, eventdensity=eventdensity, nooverlap=nooverlap
    )  # generate patients pathways
    Ph_ = genVkWithTemporalWindow(
        N, R_hidden, Tw_hidden
    )  # generate temporal phenotypes

    # redefinition of six phenotypes to ensure redundant events
    Ph_[0] = torch.zeros((N, Tw_hidden))
    Ph_[0, 3, :] = torch.ones(Tw_hidden)

    Ph_[1] = torch.zeros((N, Tw_hidden))
    Ph_[1, 5, :] = torch.ones(Tw_hidden)
    Ph_[1, 7, :] = torch.ones(Tw_hidden)

    Ph_[2] = torch.zeros((N, Tw_hidden))
    Ph_[2, 9, 0] = 1
    Ph_[2, 9, 1] = 1

    Ph_[3] = torch.zeros((N, Tw_hidden))
    Ph_[3, 11, 1] = 1
    Ph_[3, 11, 2] = 1

    Ph_[4] = torch.zeros((N, Tw_hidden))
    Ph_[4, 13, :] = torch.ones(Tw_hidden)
    Ph_[4, 15, 2] = 1

    Ph_[5] = torch.zeros((N, Tw_hidden))
    Ph_[5, 17, :] = torch.ones(Tw_hidden)
    Ph_[5, 17, 0] = 1

    dataset = []
    for p in range(K):
        # create a tensor of windows
        dec = torch.tensordot(W_[p], Ph_, dims=([0], [0]))

        # now ... windows have to be summed
        Yp = torch.zeros((N, T))
        for i in range(T - Tw_hidden + 1):
            Yp[:, i : (i + Tw_hidden)] += dec[i]

        dataset.append(Yp)

    dataset = torch.stack(dataset)
    if truncate:
        dataset.data[dataset > 1] = 1  # truncate values greater than 1

    # return Uk, Vk, Y
    X = [dataset[i] for i in range(dataset.shape[0])]

    return W_, Ph_, X, params


def gen_synthetic_data(
    K=100,
    N=20,
    T=6,
    R_hidden=4,
    Tw_hidden=3,
    sliding_window=False,
    noise=0.0,
    truncate=True,
    phenodensity = 0.5, 
    **_kwargs
):
    """
    Params
    - K: number of patients
    - N: number of medical events
    - T: length of time's stay
    - R: number of phenotype
    - Tw: length of time's window
    - sliding_window: bool, if true genUkwithSlidingTemporalWindow
    - noise: float, if >0 add a normal noise to the generated tensor
    - truncate: boolean, binarize the data
    - phenodensity: float, probability to have a phenotype at a given instant
    Returns
    - W_  third-order tensor representing pathways
    - Ph_: third-order tensor prepresenting phenotyes or a matrice of size R x N
    - X: a list of K matrices of size  N x T
    - params: a dictionary containing the parameters used to generate the dataset
    """

    params = {}
    params[
        "source"
    ] = "synthetic"  # this indicates the source of the dataset (its name, version of the generator, etc.)
    params["date"] = datetime.now()  # keep the date at which it has been generated
    # matrix dimensions
    params["K"] = K
    params["N"] = N
    params["T"] = T
    # synthetic data parameters
    params["sliding_window"] = sliding_window
    params["R_hidden"] = R_hidden
    params["Tw_hidden"] = Tw_hidden
    params["noise"] = noise
    params["truncate"] = truncate
    params["phenodensity"] = phenodensity

    if sliding_window:
        W_, Ph_, dataset = genTensor3DWithSlidingTemporalWindow(
            K, N, T, R_hidden, Tw_hidden, noise, truncate, phenodensity=phenodensity
        )
        X = [dataset[i] for i in range(dataset.shape[0])]

    else:
        W_, Ph_, dataset = genTensor4DWithTemporalWindow(
            K, N, T, R_hidden, Tw_hidden, noise, truncate
        )
        X = [
            torch.cat(tuple([dataset[i][j] for j in range(dataset.shape[1])]), axis=1)
            for i in range(dataset.shape[0])
        ]

    if Tw_hidden == 1:
        Ph_ = torch.squeeze(Ph_, 2)

    return W_, Ph_, X, params


def AddNoise_deleteEvents(dataset, proba):
    """
    params :
    - dataset : a list of matrices describing patients data
    - proba : probability of deleting an event
    """
    # clone the dataset
    noisy_dataset = [matrix.clone() for matrix in dataset]

    # Store the number of events effectively deleted
    nb_deleted_events = 0

    # Browse all events for all patients and delete them with a probability of proba
    for matrix in noisy_dataset:
        for n in range(matrix.shape[0]):
            for t in range(matrix.shape[1]):
                if np.random.binomial(n=1, p=proba, size=1)[0]:
                    if matrix[n][t]:
                        nb_deleted_events += 1
                    matrix[n][t] = 0

    return noisy_dataset, nb_deleted_events


def drawAnIndexFromProbabilites(probabilities):
    """
    Params:
        - probabilities : a list of proba for indexes
    Returns
        - an index
    """

    if sum(probabilities) > 1:
        raise Exception("Les probabilités sont invalides.")

    # générer un nombre aléatoire entre 0 et 1
    r = random.uniform(0, 1)

    # utiliser les probabilités cumulatives pour déterminer la valeur aléatoire
    cumulative_prob = 0
    for i in range(len(probabilities)):
        cumulative_prob += probabilities[i]
        if r < cumulative_prob:
            return i + 2

    raise Exception("Les probabilités sont invalides.")


def AddNoise_addEvents_Poisson(dataset, proba):
    """
    params :
    - dataset : a list of matrices describing patients data
    - proba : parameter of a Poisson law used to draw the number of events to add
    """

    # clone the dataset
    noisy_dataset = [matrix.clone() for matrix in dataset]
    rng = np.random.default_rng()

    # Store the number of events effectively adde
    number_of_added_events = 0

    for matrix in noisy_dataset:
        number_of_events_to_add = rng.poisson(proba, 1)[0]
        for i in range(number_of_events_to_add):
            m = random.randint(0, matrix.shape[0] - 1)  # draw a medical event to delete
            t = random.randint(0, matrix.shape[1] - 1)  # draw an instant

            attempt = 0
            while (
                matrix[m][t] != 0 and attempt < 5
            ):  # if it exists, try another location
                m = random.randint(0, matrix.shape[0] - 1)
                t = random.randint(0, matrix.shape[1] - 1)

            if matrix[m][t] == 0:
                number_of_added_events += 1

            matrix[m][t] = 1

    return noisy_dataset, number_of_added_events


def AddNoise_addEvents(dataset, probabilities):
    """
    params :
    - dataset : a list of matrices describing patients data
    - proba : a list of probabilities for deleting an "index" events
    """

    # clone the dataset
    noisy_dataset = [matrix.clone() for matrix in dataset]

    # Store the number of events effectively added
    number_of_added_events = 0

    for matrix in noisy_dataset:
        number_of_events_to_add = drawAnIndexFromProbabilites(probabilities)
        for i in range(number_of_events_to_add):
            m = random.randint(0, matrix.shape[0] - 1)  # draw a medical event to delete
            t = random.randint(0, matrix.shape[1] - 1)  # draw an instant

            if matrix[m][t] == 0:
                number_of_added_events += 1

            matrix[m][t] = 1

    return noisy_dataset, number_of_added_events
