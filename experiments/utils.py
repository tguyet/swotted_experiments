import torch


def success_rate(base, result):
    """
    Params:
    - base: tensor (phenotypes, pathways or patients matrices)
    - result: tensor with the same shape as base
    """
    dis = 0
    max = 0
    for i in range(len(base)):
        dis += torch.norm((base[i] - result[i]), p="fro").item()
        max += torch.norm(base[i], p="fro").item()

    try:
        return 1 - dis / max
    except ZeroDivisionError:
        return 0
