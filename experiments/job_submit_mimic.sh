#!/bin/bash

exp="EXP5_MIMIC_$(date +%F)_$(date +%s)"

mkdir -p "$exp/"
file="$exp/results.csv"

echo -e "it,R_hidden,Tw_hidden,K,N,T,R,Tw,sliding_window,noise,truncate,loss,model,non_negativity,normalization,sparsity,temp_reg,pheno_succession,error_Ph,error_W_train,error_X_train,time,error_W_test,error_X_test " >> $file


#### Useless parameters
K=100
N=20
T=6
Rhidden=8
Twhidden=1 # mandatory for CNTF and SWIFT in order to be able to compute the FIT
R=12
Tw=1

noise='False'
truncate='True'
loss='Bernoulli'
nonnegativity='True'
sparsity='True'
normalization='True'
temporalregularization='False'
phenotypesuccession='True'
batchsize=50
epochs=800

dataset="../data/mimic_data_tuple.pkl"
dataset_logpar="../../mimic-iv/mimic_data_logpar.pkl"


for it in {1..1}
do  
    for model in CNTF #SlidingWindow2 SlidingWindow3 SWIFT CNTF
    do 
        if [ "$model" = "SlidingWindow4" ]; then
            Tw=4
            model='SlidingWindow'
            echo "running SWoTTeD with Tw=$Tw..." 
            res=$(python3 experiments_run_model.py -it $it -m $model -l $loss -p $dataset -dir $exp -r $R -tw $Tw -b $batchsize -e $epochs -sp -nn -nr -ps -mi) 
            mv $exp/model.pkl $exp/model_${model}_${Tw}_${it}.pkl
        elif [ "$model" = "SlidingWindow2" ]; then
            Tw=2
            model='SlidingWindow'
            echo "running SWoTTeD with Tw=$Tw..." 
            res=$(python3 experiments_run_model.py -it $it -m $model -l $loss -p $dataset -dir $exp -r $R -tw $Tw -b $batchsize -e $epochs -sp -nn -nr -ps -mi)
            mv $exp/model.pkl $exp/model_${model}_${Tw}_${it}.pkl
        elif [ "$model" = "SlidingWindow3" ]; then
            Tw=3
            model='SlidingWindow'
            echo "running SWoTTeD with Tw=$Tw..." 
            res=$(python3 experiments_run_model.py -it $it -m $model -l $loss -p $dataset -dir $exp -r $R -tw $Tw -b $batchsize -e $epochs -sp -nn -nr -ps -mi)  
            mv $exp/model.pkl $exp/model_${model}_${Tw}_${it}.pkl
        elif [ "$model" = "SlidingWindow4" ]; then
            Tw=4
            model='SlidingWindow'
            echo "running SWoTTeD with Tw=$Tw..." 
            res=$(python3 experiments_run_model.py -it $it -m $model -l $loss -p $dataset -dir $exp -r $R -tw $Tw -b $batchsize -e $epochs -sp -nn -nr -ps -mi)  
            mv $exp/model.pkl $exp/model_${model}_${Tw}_${it}.pkl
        elif [ "$model" = "SlidingWindow5" ]; then
            Tw=5
            model='SlidingWindow'
            echo "running SWoTTeD with Tw=$Tw..." 
            res=$(python3 experiments_run_model.py -it $it -m $model -l $loss -p $dataset -dir $exp -r $R -tw $Tw -b $batchsize -e $epochs -sp -nn -nr -ps -mi)  
            mv $exp/model.pkl $exp/model_${model}_${Tw}_${it}.pkl
        elif [ "$model" = "CNTF" ]; then
            echo "running CNTF..." 
            res=$(python3 ../competitors/cntf/run_cntf.py -it $it -p $dataset -r $R -dir $exp)
            mv $exp/model.pkl $exp/model_${model}_${Tw}_${it}.pkl 
        elif [ "$model" = "SWIFT" ]; then
            echo "running SWIFT..." 
            res=$(python3 ../competitors/swift/run_swift.py -it $it -p $dataset -r $R --name $exp)
        elif  [ "$model" = "LogPar" ]; then
            echo "running LogPar..." 
            res=$(python3 ../competitors/LogPar/run_logpar.py -it $it --data_path $dataset_logpar --rank $R --name $exp -m)
        fi

        echo -e "$it,$Rhidden,$Twhidden,$K,$N,$T,$R,$Tw,$slidingwindow,$noise,$truncate,$loss,$model,$nonnegativity,$normalization,$sparsity,$temporalregularization,$phenotypesuccession, $res" >> $file
    done
done
