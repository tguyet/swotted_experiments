#!/bin/bash
exp="expe_uniqueness/EXP_$(date +%F)_$(date +%s)"

mkdir -p "$exp/"
file="$exp/results.csv"

cp $0 $exp

echo -e "it,R_hidden,Tw_hidden,K,N,T,R,Tw,loss,model,normalization,sparsity,pheno_nonsuccession,error_Ph,error_W_train,error_X_train,time,error_W_test,error_X_test" >> $file

# default synthetic dataset parameters
K=500
N=20
T=10
Rhidden=4
Twhidden=1 # 1 is mandatory for comparison with LogPar, CNTF and SWIFT in order to be able to compute the FIT

# default models' parameters
R=4
Tw=${Twhidden}
loss='Bernoulli'
normalization='True'
phenotypesuccession=0.5
sparsity=2
batchsize=50
epochs=100

for R in 4 12 20 36
do
Rhidden=$R
for datait in {1..8}
do
    # Generate a synthetic dataset
    dataset="$exp/data_${datait}_sw.pickle"
    python3 experiments_gen_data.py -k $K -n $N -t $T -r $Rhidden -tw $Twhidden -tr -sw -p $dataset

    # repeat several time each model on the same data
    for it in {1..8}
    do
        # Run the different models on the generated dataset
        for model in SWoTTeD CNTF
        do 
            if [ "$model" = "SWoTTeD" ]; then
                echo "running SWoTTeD..." 
                cmd="../competitors/run_swotted.py -it $it -l $loss -p $dataset -r $R -tw $Tw -b $batchsize -e $epochs -sp $sparsity -ps $phenotypesuccession -dir ."
                if [ "$normalization" = "True" ]; then
                    cmd="${cmd} -nr"
                fi
                res=$(python3 $cmd)
                
                #save the model
                cp model.pkl $exp/model_swotted_R${R}_data${datait}_it${it}.pkl
                
            elif [ "$model" = "CNTF" ]; then
                echo "running CNTF..." 
                res=$(python3 ../competitors/cntf/run_cntf.py -it $it -p $dataset -r $R -e $epochs -dir .) 

                #save the model
                cp model.pkl $exp/model_cntf_R${R}_data${datait}_it${it}.pkl
            fi

            echo -e "$it,$Rhidden,$Twhidden,$K,$N,$T,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession, $res" >> $file
        done

    done

done
done
