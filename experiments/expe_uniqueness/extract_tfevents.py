
# require tensorflow (pip install tensorflow ... not installed in poetry!)

from tensorflow.python.summary.summary_iterator import summary_iterator
event_file = "/home/tguyet/Progs/swotted_experiments/experiments/lightning_logs/version_43/events.out.tfevents.1690507224.portable-tg.228244.0"

for e in summary_iterator(event_file):
    print("-------")
    for v in e.summary.value:
        print(v.tag)
        #if v.tag == 'loss' or v.tag == 'accuracy':
        #    print(v.simple_value)
        print(v.simple_value)
