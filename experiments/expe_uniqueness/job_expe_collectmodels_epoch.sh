#!/bin/bash
exp="EXP_$(date +%F)_$(date +%s)"

mkdir -p "$exp/"
file="$exp/results.csv"

echo -e "it,epochs,R_hidden,Tw_hidden,K,N,T,R,Tw,loss,model,normalization,sparsity,pheno_nonsuccession,error_Ph,error_W_train,error_X_train,time,error_W_test,error_X_test" >> $file

# default synthetic dataset parameters
K=500
N=20
T=10
Rhidden=12
Twhidden=3 # 1 is mandatory for comparison with LogPar, CNTF and SWIFT in order to be able to compute the FIT

# default models' parameters
R=${Rhidden}
Tw=${Twhidden}
loss='Bernoulli'
normalization='True'
phenotypesuccession=0.25
sparsity=0.25
batchsize=50
epochs=1000 #set a maximum epochs ... and we collect later data from the torchlighting log files

for datait in {1..5}
do
    # Generate a synthetic dataset
    dataset="$exp/data_${datait}_sw.pickle"
    python3 experiments_gen_data.py -k $K -n $N -t $T -r $Rhidden -tw $Twhidden -tr -sw -p $dataset

    for epochs in 100 300 500 700
    do
        # Repeat several times each model on the same data
        for it in {1..7}
        do
            # Run the different models on the generated dataset
            for model in fastSWoTTeD SWoTTeD
            do 
                if [ "$model" = "SWoTTeD" ]; then
                    echo "running SWoTTeD..." 
                    cmd="../competitors/run_swotted.py -it $it -l $loss -p $dataset -r $R -tw $Tw -b $batchsize -e $epochs -sp $sparsity -ps $phenotypesuccession -dir ."
                    if [ "$normalization" = "True" ]; then
                        cmd="${cmd} -nr"
                    fi
                    res=$(python3 $cmd)
                    
                    #save the model
                    mv model.pkl $exp/model_swotted_e${epochs}_data${datait}_it${it}.pkl
                elif [ "$model" = "fastSWoTTeD" ]; then
                    echo "running fastSWoTTeD..." 
                    cmd="../competitors/run_fastswotted.py -it $it -l $loss -p $dataset -r $R -tw $Tw -b $batchsize -e $epochs -sp $sparsity -ps $phenotypesuccession -dir ."
                    if [ "$normalization" = "True" ]; then
                        cmd="${cmd} -nr"
                    fi
                    res=$(python3 $cmd)
                    
                    #save the model
                    mv model.pkl $exp/model_fastswotted_e${epochs}_data${datait}_it${it}.pkl
                fi

                echo -e "$it,$epochs,$Rhidden,$Twhidden,$K,$N,$T,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession, $res" >> $file
            done
        done
    done
done
