rm(list=ls())
library(readr)
library(reshape2)
require(ggplot2)
library(dplyr)
library(tidyr)
library(stringr)
library(latex2exp)
library(kableExtra)

#function required for legend generation
require(gridExtra)
g_legend <- function(a.gplot){
  tmp<- ggplot_gtable(ggplot_build(a.gplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)
}

# if export legend is True, then, a legend is generated in a separated PDF file (and figure does not have a proper legend)
export_legend=F

############# Load the dataset################"

wd = "/home/tguyet/Doctorants/Hana/Stage/temporal_phenotyping/Code/temp_phenotyping/experiments/expe_uniqueness"
setwd(wd)

results <- read_csv("sim_results.csv",show_col_types = FALSE)
divresults <- read_csv("div_results.csv",show_col_types = FALSE)

#########  Frobenius (boxplot) #####

data = results%>%filter(dist=="fro")

data$Tw <- as.factor(data$Tw)

p <- ggplot(data, mapping=aes(x=Tw, y=sim))
p <- p + facet_grid(~R, scales="free")
p <- p + geom_boxplot()
p <- p + geom_point(size=.5)
p <- p + xlab("Windows size") + ggtitle("Dissimilarities between SWoTTeD runs") + ylab("Dissimilarity (L2)")
p <- p + theme_bw()
p <- p + theme(strip.background = element_rect(colour="black", fill="white", linewidth=0), 
               legend.title=element_blank(),
               panel.grid.major = element_blank())

if(export_legend) {
  p <- p + theme(legend.position="none")
}
p


#########  Frobenius (boxplot), R=20 #####

data = results%>%filter(dist=="fro" & R==20)

data$Tw <- as.factor(data$Tw)

p <- ggplot(data, mapping=aes(x=Tw, y=sim))
#p <- p + facet_grid(~R, scales="free")
p <- p + geom_boxplot()
p <- p + geom_point(size=.5)
p <- p + xlab("Windows size/CNTF") + ggtitle("Dissimilarities between SWoTTeD runs (R=20)") + ylab("Frobenius distance")
p <- p + theme_bw()
p <- p + theme(strip.background = element_rect(colour="black", fill="white", linewidth=0), 
               legend.title=element_blank(),
               panel.grid.major = element_blank())

if(export_legend) {
  p <- p + theme(legend.position="none")
}
p

ggsave(filename="Frobenius.pdf", plot=p, width = 6, height = 2)

#########  Cosinus (boxplot) #####

data = results%>%filter(dist=="cos")

data$Tw <- as.factor(data$Tw)
#data = data %>% mutate(R=paste0("R=", R))

labs <- c("R=8", "R=12", "R=20")
names(labs) <- c(8, 12, 20)

p <- ggplot(data, mapping=aes(x=Tw, y=sim))
p <- p + facet_grid(~R, scales="free", labeller = labeller(R = labs))
p <- p + geom_boxplot()
p <- p + geom_point(size=.5)
p <- p + xlab("Windows size/CNTF") + ylab("Cosinus similarity")
#p <- p + ggtitle("Similarities between SWoTTeD runs")
p <- p + theme_bw()
p <- p + theme(strip.background = element_rect(colour="black", fill="white", linewidth=0), 
               legend.title=element_blank(),
               panel.grid.major = element_blank())

if(export_legend) {
  p <- p + theme(legend.position="none")
}
p

ggsave(filename="sim_runs_cos.pdf", plot=p, width = 9, height = 3)

#########  Cosine (boxplot), R=20 #####

data = results%>%filter(dist=="cos" & R==20)

data$Tw <- as.factor(data$Tw)

p <- ggplot(data, mapping=aes(x=Tw, y=sim))
#p <- p + facet_grid(~R, scales="free")
p <- p + geom_boxplot()
p <- p + geom_point(size=.5)
p <- p + xlab("Windows size/CNTF") + ylab("Cosinus similarity")
#p <- p + ggtitle("Similarities between SWoTTeD runs (R=20)")
p <- p + theme_bw()
p <- p + theme(strip.background = element_rect(colour="black", fill="white", linewidth=0), 
               legend.title=element_blank(),
               panel.grid.major = element_blank())

if(export_legend) {
  p <- p + theme(legend.position="none")
}
p


ggsave(filename="Cosine.pdf", plot=p, width = 6, height = 3)

table <- data %>% select(it, Tw, sim) %>% group_by(Tw) %>%
  summarise(Mean=round(mean(sim),digits=2),
            SD=round(sd(sim),digits=1)
  )

table %>%
  kbl(caption="Cosinus similarity between different runs",
      format= "latex",
      col.names = c("Window size","Mean","SD"),
      align="r") %>%
  kable_minimal(full_width = F, html_font = "helvetica")



# if(export_legend) {
#   # Generate legend (to be saved manually)
#   p <- p + theme(text = element_text(size=12),legend.position = "bottom") + guides(color=guide_legend(title=""),shape=guide_legend(title=""),linetype=guide_legend(title=""))
#   legend <- g_legend(p)
#   grid.arrange(legend)
#   legend
#   ggsave(filename="legend_diag.pdf", plot=legend, width = 5.8, height = 0.4)
# }


#########  Diversity Frobenius (boxplot) #####

data = divresults%>%filter(dist=="fro")

data$Tw <- as.factor(data$Tw)

labs <- c("R=8", "R=12", "R=20")
names(labs) <- c(8, 12, 20)

p <- ggplot(data, mapping=aes(x=Tw, y=div))
p <- p + facet_grid(~R, scales="free", labeller = labeller(R = labs))
p <- p + geom_boxplot()
p <- p + geom_point(size=.5)
p <- p + xlab("Windows size/CNTF") + ylab("Dissimilarity (L2)")
#p <- p + ggtitle("Diversity of phenotypes")
p <- p + theme_bw()
p <- p + theme(strip.background = element_rect(colour="black", fill="white", linewidth=0), 
               legend.title=element_blank(),
               panel.grid.major = element_blank())

if(export_legend) {
  p <- p + theme(legend.position="none")
}
p


#########  Diversity Cosine (boxplot) #####

data = divresults%>%filter(dist=="cos")

data$Tw <- as.factor(data$Tw)

labs <- c("R=8", "R=12", "R=20")
names(labs) <- c(8, 12, 20)

p <- ggplot(data, mapping=aes(x=Tw, y=div))
p <- p + facet_grid(~R, scales="free")
p <- p + geom_boxplot()
p <- p + geom_point(size=.5)
p <- p + xlab("Windows size/CNTF") + ylab("Cosine simmilarity")
#p <- p + ggtitle("Diversity of phenotypes")
p <- p + theme_bw()
p <- p + theme(strip.background = element_rect(colour="black", fill="white", linewidth=0), 
               legend.title=element_blank(),
               panel.grid.major = element_blank())

if(export_legend) {
  p <- p + theme(legend.position="none")
}
p

ggsave(filename="diversity_cos.pdf", plot=p, width = 9, height = 3)
