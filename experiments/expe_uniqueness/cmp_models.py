#!python
import pickle
import numpy as np
import torch
from munkres import Munkres
import sys,os
import warnings
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))

#required for pickle import:
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../../competitors')))

import glob

def phenotype_dissimilarity_frobenius(phenotype_1,phenotype_2):
    n1= torch.norm(phenotype_1, p='fro').item()
    n2= torch.norm(phenotype_2, p='fro').item()
    if n1==0 and n2==0:
        return 1
    elif n1==0:
        return n2
    elif n2==0:
        return n1
    else:
        return torch.norm((phenotype_1-phenotype_2), p='fro').item()/(n1*n2)

def phenotype_similarity_cosinus(phenotype_1,phenotype_2):
    norm1=torch.sqrt(torch.sum(torch.mm(phenotype_1.T,phenotype_1).diag()))
    norm2=torch.sqrt(torch.sum(torch.mm(phenotype_2.T,phenotype_2).diag()))
    if(norm1*norm2==0):
        if norm1==norm2:
            return 0
        else:
            return 1
    dotprod=torch.sum(torch.mm(phenotype_1.T,phenotype_2).diag())
    meancos=  float((dotprod/(norm1*norm2)).detach())
    #print(meancos)
    
    if(meancos>1):
        print("WARNING")
        meancos=1
    return meancos

def diversity(phenotypes, dist='fro'):
    costs = 0
    for i in range(phenotypes.shape[0]):
        for j in range(i+1,phenotypes.shape[0]):
            if dist=='fro':
                costs += phenotype_dissimilarity_frobenius(phenotypes[i],phenotypes[j])
            elif dist=='cos':
                costs += 1-phenotype_similarity_cosinus(phenotypes[i],phenotypes[j])
            else:
                warnings.warn("wrong argument for distance type")
                return 0
    return costs/(phenotypes.shape[0]*(phenotypes.shape[0]-1))

def compute_costs(phenotypes_1,phenotypes_2, dist='fro'):
    #cost matrix
    costs = np.zeros((phenotypes_1.shape[0], phenotypes_2.shape[0]))

    for i in range(phenotypes_1.shape[0]):
        for j in range(phenotypes_2.shape[0]):
            if dist=='fro':
                costs[i][j] = phenotype_dissimilarity_frobenius(phenotypes_1[i],phenotypes_2[j])
            elif dist=='cos':
                costs[i][j] = 1-phenotype_similarity_cosinus(phenotypes_1[i],phenotypes_2[j])
            else:
                warnings.warn("wrong argument for distance type")
                return 0
    m = Munkres() # Use of Hungarian Algorithm to find phenotypes correspondances
    indexes = m.compute(costs.copy())
    if dist=='fro':
        return np.mean([ costs[i] for i in indexes]),indexes,costs
    elif dist=='cos':
        return np.mean([ 1-costs[i] for i in indexes]),indexes,costs


if __name__ == '__main__':
    location="/home/tguyet/Progs/swotted_experiments/experiments/EXP_RWD_ESHOP/"

    # similarity of phenotypes
    fout = open(location+"results_sim.csv","w")

    print("similarities")
    fout.write("it,R,dist,dataset,sim,method\n")
    for R in [4,12,36]:
        for Tw in [1,3,5]:
            #for e in [100,300,500,700]:
            #print(f"\tR={R}")
            for dataset in range(10):
                print(f"\t\tdataset={dataset}")
                for model in ["swotted", "fastswotted", "cntf"]:
                    for dist in ['cos','fro']:
                        phenotypes=[]
                        #for filename in glob.glob(location+f'models_epochs/model_{model}_e{e}_data{dataset+1}_*.pkl'):
                        #for filename in glob.glob(location+f'models_Tw/model_{model}_Tw{Tw}_data{dataset+1}_*.pkl'):
                        #for filename in glob.glob(location+f'models_R/model_{model}_R{R}_data{dataset+1}_*.pkl'):
                        for filename in glob.glob(location+f'models_R/model_{model}_{R}_{Tw}_{dataset+1}_*.pkl'):
                            m=pickle.load(open(filename,"rb"))
                            if model=="cntf":
                                phenotypes.append(torch.unsqueeze(m,2))
                            else:
                                phenotypes.append(m.Ph)
                            
                        similarities=[]
                        for i,psi in enumerate(phenotypes):
                            for j,psj in enumerate(phenotypes):
                                if i<j:
                                    s,I,C=compute_costs(psi,psj, dist=dist)
                                    similarities.append(s)

                        for it,sim in enumerate(similarities):
                            fout.write(str(it)+","+str(e)+","+dist+","+str(dataset+1)+","+str(sim)+","+model+"\n")
    fout.close()

    # diversity of phenotypes
    fout = open(location+"results_div.csv","w")
    fout.write("it,R,dist,dataset,div,method\n")
    print("diversity")
    for R in [4,12,36]:
        for Tw in [1,3,5]:
            #print(f"\tR={R}")
            for dataset in range(10):
                print(f"\t\tdataset={dataset}")
                for model in ["swotted", "fastswotted", "cntf"]:
                    phenotypes=[]
                    for filename in glob.glob(location+f'models_epochs/models_R/model_{model}_{R}_{Tw}_{dataset+1}_*.pkl'):
                    #for filename in glob.glob(location+f'models_Tw/model_{model}_Tw{Tw}_data{dataset+1}_*.pkl'):
                    #for filename in glob.glob(location+f'models_R/model_{model}_R{R}_data{dataset+1}_*.pkl'):
                        m=pickle.load(open(filename,"rb"))
                        if model=="cntf":
                            phenotypes.append(torch.unsqueeze(m,2))
                        else:
                            phenotypes.append(m.Ph)

                    for dist in ['cos','fro']:
                        for i,psi in enumerate(phenotypes):
                            s=diversity(psi, dist=dist)
                            fout.write(str(i)+","+str(e)+","+dist+","+str(dataset+1)+","+str(s)+","+model+"\n")
    fout.close()
