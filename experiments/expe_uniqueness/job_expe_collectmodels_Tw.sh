#!/bin/bash
exp="EXP_$(date +%F)_$(date +%s)"

mkdir -p "$exp/"
file="$exp/results.csv"

echo -e "it,R_hidden,Tw_hidden,K,N,T,R,Tw,loss,model,normalization,sparsity,pheno_nonsuccession,error_Ph,error_W_train,error_X_train,time,error_W_test,error_X_test" >> $file

# default synthetic dataset parameters
K=500
N=20
T=10
Rhidden=8
Twhidden=1 # 1 is mandatory for comparison with LogPar, CNTF and SWIFT in order to be able to compute the FIT

# default models' parameters
R=8
Tw=${Twhidden}
loss='Bernoulli'
normalization='True'
phenotypesuccession=0.25
sparsity=0.25
batchsize=50
epochs=100

for Tw in 1 2 4 6
do
Twhidden=$Tw
for datait in {1..10}
do
    # Generate a synthetic dataset
    dataset="$exp/data.pickle"
    python3 experiments_gen_data.py -k $K -n $N -t $T -r $Rhidden -tw $Twhidden -tr -sw -p $dataset

    # repeat several time each model on the same data
    for it in {1..10}
    do
        # Run the different models on the generated dataset
        for model in fastSWoTTeD SWoTTeD #CNTF
        do 
            if [ "$model" = "SWoTTeD" ]; then
                echo "running SWoTTeD..." 
                cmd="../competitors/run_swotted.py -it $it -l $loss -p $dataset -r $R -tw $Tw -b $batchsize -e $epochs -sp $sparsity -ps $phenotypesuccession -dir ."
                if [ "$normalization" = "True" ]; then
                    cmd="${cmd} -nr"
                fi
                res=$(python3 $cmd)
                
                #save the model
                cp model.pkl $exp/model_swotted_Tw${Tw}_data${datait}_it${it}.pkl
                
    	    elif [ "$model" = "fastSWoTTeD" ]; then
                echo "running fastSWoTTeD..." 
		        cmd="../competitors/run_fastswotted.py -it $it -l $loss -p $dataset -r $R -tw $Tw -b $batchsize -e $epochs -sp $sparsity -ps $phenotypesuccession -dir ."
		        if [ "$normalization" = "True" ]; then
			        cmd="${cmd} -nr"
		        fi
		        res=$(python3 $cmd)
		        
                #save the model
                cp model.pkl $exp/model_fastswotted_Tw${Tw}_data${datait}_it${it}.pkl
            elif [ "$model" = "CNTF" ]; then
                echo "running CNTF..." 
                res=$(python3 ../competitors/cntf/run_cntf.py -it $it -p $dataset -r $R -dir .) 

                #save the model
                cp model.pkl $exp/model_cntf_Tw${Tw}_data${datait}_it${it}.pkl
            fi

            echo -e "$it,$Rhidden,$Twhidden,$K,$N,$T,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession, $res" >> $file
        done

    done

done
done
