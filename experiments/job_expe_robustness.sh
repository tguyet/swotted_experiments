#!/bin/bash

exp="EXP_Noisy_Data_$(date +%F)_$(date +%s)"

mkdir -p "$exp/"
file="$exp/results.csv"

cp $0 $exp

echo -e "it,R_hidden,Tw_hidden,K,N,T,R,Tw,loss,model,normalization,sparsity,pheno_succession,event,proba,nb_noisy_events,nb_events,error_Ph,error_W_train,error_X_train,time,error_W_test,error_X_test" >> $file

#syntheric data parameters
K=400
test_ratio=0.3
N=20
T=10
Rhidden=6
Twhidden=3 

#model parameters
R=$Rhidden
Tw=$Twhidden
loss='Bernoulli'
sparsity=2
normalization='True'
phenotypesuccession=0.5
model='SWoTTeD'
batchsize=50
epochs=200

for it in {1..20}
do  
    dataset="$exp/data_${it}.pickle"
    python3 experiments_gen_data.py -k $K -n $N -t $T -r $Rhidden -tw $Twhidden -tr -sw -p $dataset -tt $test_ratio
    
    
    for p in 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7
    do 
        echo "running SWoTTeD..." 

        event='Delete'
        
        resdata=$(python3 noisify.py -p $dataset -pro $p -de)
        
        cmd="../competitors/run_fastswotted.py -it $it -l $loss -p $dataset.noisy -r $R -tw $Tw -b $batchsize -e $epochs -sp $sparsity -ps $phenotypesuccession --noshuffle"
    	if [ "$normalization" = "True" ]; then
                cmd="${cmd} -nr"
	    fi
        res=$(python3 $cmd)

        echo -e "$it,$Rhidden,$Twhidden,$K,$N,$T,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession,$event,$p,$resdata,$res" >> $file
    done

    for p in 2 5 10 15 20 25
    do 
        echo "running SWoTTeD..." 
        event='Add'
        
        resdata=$(python3 noisify.py -p $dataset -pro $p -ae)
        
        cmd="../competitors/run_fastswotted.py -it $it -l $loss -p $dataset.noisy -r $R -tw $Tw -b $batchsize -e $epochs -sp $sparsity -ps $phenotypesuccession --noshuffle"
    	if [ "$normalization" = "True" ]; then
                cmd="${cmd} -nr"
	    fi
        res=$(python3 $cmd)         
    
        echo -e "$it,$Rhidden,$Twhidden,$K,$N,$T,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession,$event,$p,$resdata,$res" >> $file
    done

done

