{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Cohort construction from the MIMIC-IV database\n",
    "\n",
    "The objective of this notebook is to construct a cohort of adult patients from the MIMIC-IV database in order to evaluate tensor decomposition methods such as [SWoTTeD](https://gitlab.inria.fr/hsebia/swotted) or [CNTF](https://github.com/jakeykj/cntf). By providing this notebook, we attempt to foster fair comparison between care pathway analysis methods. \n",
    "\n",
    "The scenario that has been chosen is inspired by COVID19 studies in which the objective was to predict the use of mechanical ventilation. In our tensor decomposition we would like to describe the pathways of patients being ventilated, such that they can be charaterized.\n",
    "\n",
    "This cohort includes patients from the MIMIC database being mechanically ventilated and it describes their pathway by the delivered cares occuring before the first ventilation act. \n",
    "The visits for which a patient has been ventilated the first or the second day are discarded because too short."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exploratory analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let start by installing and loading the required modules for this notebook. \n",
    "We use two libraries:\n",
    "- `psycopg` to handle Postgresql queries (note that under Linux, this requires to install `libpq-dev`)\n",
    "- `pandas` to manage the datasets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# installation of the required packages\n",
    "!pip3 install pandas\n",
    "!pip3 install psycopg2\n",
    "!pip3 install seaborn"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import pandas.io.sql as sqlio\n",
    "import seaborn as sns\n",
    "import matplotlib.pyplot as plt\n",
    "import psycopg2\n",
    "import pickle\n",
    "from datetime import datetime "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now open a connection to the database ... Do not forget to close the connection when you finished to play with this notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Specify the right informations to connect to the database\n",
    "pguser=\"mimicuser\"\n",
    "pgpassword=\"mimic\"\n",
    "dbname=\"mimic\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    conn = psycopg2.connect(f'dbname={dbname} user={pguser} password={pgpassword}')\n",
    "    cur = conn.cursor()\n",
    "    cur.execute(\"SET search_path TO mimiciv\")\n",
    "except (Exception, psycopg2.Error) as error:\n",
    "    print(\"Error while opening connection to the database from PostgreSQL\", error)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Mechanical Ventilation Events"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let start by finding ICD procedure codes related to ventilation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "query = \"\"\"SELECT * FROM d_icd_procedures WHERE long_title LIKE '%ventilation%'\"\"\"\n",
    "\n",
    "res=sqlio.read_sql_query(query, conn)\n",
    "print(res)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have 4 codes related to mechanical ventilation ... at this stage, I'll take all of them to identify my event index. Then we create a new query to identify them ... note that the idea is to modify the later the definition of the index event easily."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "query_index_eventtype = \"\"\"SELECT icd_code FROM d_icd_procedures WHERE long_title LIKE '%ventilation%'\"\"\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So now ... We evaluate the number of such events! At the same time, we also count the \"visits\" having such events, and also the patients! "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "query = f\"\"\"SELECT count(*), count(DISTINCT hadm_id), count(DISTINCT subject_id) FROM procedures_icd WHERE icd_code in ({query_index_eventtype})\"\"\"\n",
    "res=sqlio.read_sql_query(query, conn)\n",
    "print('Number of ventilation events: %d'%(res['count'].iloc[0,0]))\n",
    "print('Number of admissions with ventilation event: %d'%(res.iloc[0,1]))\n",
    "print('Number of patients with ventilation event: %d'%(res.iloc[0,1]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that are may have several ventilation events during the same visit ... this means that we have to take care of the definition of our event index: it has to be, for instance, the first occurrence of the event of interest during some adminission.\n",
    "Here .. it seems that each patient being ventilated had only one admission."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dataset construction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We first look for the occurrences of our events of interest (ventilation) with two restrictions:\n",
    "* we only consider ventilations that started the day after the hospitalisation (not the same date)\n",
    "* we only consider the first ventilation procedure in case there are several ventilations in the same admission. This criteria is applied out of the SQL query.\n",
    "* we select only adults"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# subquery for admissions ... I take all of them to make it simple\n",
    "min_nb_days=2\n",
    "query_index_events = f\"\"\"SELECT DISTINCT Admissions.admittime::Date, procevent.hadm_id, procevent.chartdate AS IndexDate FROM procedures_icd AS procevent \n",
    "                            JOIN Admissions ON Admissions.hadm_id=procevent.hadm_id\n",
    "                            JOIN Patients ON Patients.subject_id=Admissions.subject_id\n",
    "                            WHERE Patients.anchor_year>=18 AND procevent.icd_code IN ({query_index_eventtype}) AND Admissions.admittime+INTERVAL '{min_nb_days} days'<=procevent.chartdate\"\"\"\n",
    "index_events=sqlio.read_sql_query(query_index_events, conn)\n",
    "#select only the first event if multiple events for the same admission\n",
    "first_index_events=index_events.groupby(\"hadm_id\").min().reset_index()\n",
    "print(first_index_events)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#now .. the query to collect events (based on the admission of the query above)\n",
    "query = f\"\"\"SELECT procevent.hadm_id, procedures_icd.icd_code, procevent.IndexDate, procevent.IndexDate-procedures_icd.chartdate AS delay FROM procedures_icd \n",
    "                                            JOIN ({query_index_events}) AS procevent ON procevent.hadm_id=procedures_icd.hadm_id\n",
    "                                            WHERE procedures_icd.chartdate<procevent.IndexDate\"\"\"\n",
    "procedures=sqlio.read_sql_query(query, conn)\n",
    "#select only the events for the first occurrence of the index event\n",
    "procedures=pd.merge(first_index_events, procedures, how='inner', on=['hadm_id','indexdate'])\n",
    "print(procedures)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# the same with drugs (we use the ndc National Drug Code)\n",
    "query = f\"\"\"SELECT procevent.hadm_id, prescriptions.ndc, procevent.IndexDate, prescriptions.starttime AS start_date, prescriptions.stoptime as stop_date FROM prescriptions\n",
    "                                            JOIN ({query_index_events}) AS procevent ON procevent.hadm_id=prescriptions.hadm_id\n",
    "                                            WHERE prescriptions.starttime<procevent.IndexDate\"\"\"\n",
    "res=sqlio.read_sql_query(query, conn)\n",
    "#select only the events for the first occurrence of the index event\n",
    "res=pd.merge(first_index_events, res, how='inner', on=['hadm_id','indexdate'])\n",
    "\n",
    "#remove the 0 codes that are too difficult to use\n",
    "res=res[res.ndc!=\"0\"]\n",
    "print(res)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#We remove the records with zero codes that are meaningless\n",
    "res=res.dropna()\n",
    "#res['ndc']=res['ndc'].astype(int)\n",
    "res=res.loc[res.ndc!=0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#stop_date can not be later than indexdate, so\n",
    "res['stop_date']=res[['stop_date','indexdate']].min(axis=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have to transform the data for which we have exposure periods ... into a collection of dates (it is easier to do with dates)\n",
    "\n",
    "**Warning** this step can take time ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "res_nona=res\n",
    "res_nona['start_date']=res_nona['start_date'].dt.date\n",
    "res_nona['stop_date']=res_nona['stop_date'].dt.date\n",
    "spread_drugs=pd.concat([pd.DataFrame({'date': pd.date_range(row['start_date'], row['stop_date']),\n",
    "               'hadm_id': row.hadm_id,\n",
    "               'ndc': row.ndc,\n",
    "               'indexdate' : row.indexdate,\n",
    "               'admittime' : row.admittime}, columns=['date', 'hadm_id', 'ndc', 'indexdate','admittime']) \n",
    "           for i, row in res_nona.iterrows()], ignore_index=True)\n",
    "spread_drugs['delay']=spread_drugs['indexdate']-spread_drugs['date']\n",
    "spread_drugs=spread_drugs[[\"hadm_id\",'admittime','indexdate',\"ndc\",\"delay\"]]\n",
    "spread_drugs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now have two tables with the same structure (procedures and drugs) ... the structure of the database is not fully suitable for our needs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Filtering on the 100 most used procedures\n",
    "freq_proc=procedures['icd_code'].value_counts().reset_index().iloc[0:100]['index'].tolist()\n",
    "procedures = procedures[ procedures['icd_code'].isin(freq_proc)]\n",
    "#Filtering on the 100 most used drugs\n",
    "freq_drugs=spread_drugs['ndc'].value_counts().reset_index().iloc[0:100]['index'].tolist()\n",
    "spread_drugs = spread_drugs[ spread_drugs['ndc'].isin(freq_drugs)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "procedures"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#spread_drugs['delay']=spread_drugs['delay'].dt.days\n",
    "#procedures['delay']=procedures['delay'].dt.days\n",
    "#procedures.icd_code = procedures.icd_code.astype(int)\n",
    "procedures.icd_code = \"p_\" + procedures.icd_code\n",
    "spread_drugs.ndc = \"d_\" + spread_drugs.ndc\n",
    "cc_mat=pd.concat([spread_drugs.rename(columns={'ndc':'code'}), procedures.rename(columns={'icd_code':'code'})])\n",
    "cc_mat['delay']=cc_mat['delay'].dt.days\n",
    "cc_mat['value']=np.ones(len(cc_mat))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#example of a processing to prepare the matrix of a patient (to be apply to all patients in next cells)\n",
    "\"\"\"\n",
    "x=cc_mat[cc_mat.hadm_id==29988601]\n",
    "#x['value']=np.ones(len(x))\n",
    "los=(x.iloc[0]['indexdate'].to_pydatetime().date()-x.iloc[0]['admittime']).days\n",
    "new_index=pd.MultiIndex.from_product([cc_mat.code.unique(), np.arange(0,los)], names=['code', 'delay'])\n",
    "x_rei=x[['code', 'delay','value']].drop_duplicates().set_index(['code','delay'])\n",
    "x_rei=x_rei.reindex( new_index,fill_value=0)\n",
    "x_rei\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def genMatrix(x):\n",
    "    los=(x.iloc[0]['indexdate'].to_pydatetime().date()-x.iloc[0]['admittime']).days\n",
    "    new_index=pd.MultiIndex.from_product([cc_mat.code.unique(), np.arange(0,los)], names=['code', 'delay'])\n",
    "    x_rei=x[['code', 'delay','value']].drop_duplicates().set_index(['code','delay'])\n",
    "    x_rei=x_rei.reindex( new_index,fill_value=0)\n",
    "    x_rei=x_rei.reset_index()\n",
    "    mat=pd.pivot_table(x_rei, columns='code', index=['delay'], values='value', aggfunc=np.sum)\n",
    "    return(mat)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#takes few seconds to create a dataframe that reorganize the data and complete \n",
    "mat=cc_mat.groupby('hadm_id').apply(genMatrix).fillna(0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#transformation as a list of numpy arrays\n",
    "mat=mat.reset_index()\n",
    "mat=mat.iloc[:,mat.columns!='delay'].set_index('hadm_id', drop=True)\n",
    "data=[mat.loc[i,mat.columns!='hadm_id'].to_numpy() for i in mat.index.unique()]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params={}\n",
    "params['source']=\"mimic\"        # this indicates the source of the dataset (its name, version of the generator, etc.)\n",
    "params['comments']=\"\"\"Care pathways of patients being ventilated in ICU.\"\"\"\n",
    "params['generator']=\"CohortConstruction.ipynb\"\n",
    "params['date']=datetime.now()  # keep the date at which it has been generated\n",
    "# matrix dimensions\n",
    "params['K']=len(data)                      \n",
    "params['N']=data[0].shape[1]\n",
    "params['T']=None #irregular lengths\n",
    "\n",
    "#transpose the temporal and drug dimensions\n",
    "data = [ d.transpose() for d in data]\n",
    "\n",
    "with open('mimic_data.pkl', 'wb') as f:\n",
    "    pickle.dump([None, None, data, params], f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Collect the labels"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* 100 first columns are drugs (prefixed with \"d_\")\n",
    "* 100 last columns are procedures (prefixed with \"p_\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "query = f\"\"\"SELECT DISTINCT drug, ndc FROM prescriptions\"\"\"\n",
    "res=sqlio.read_sql_query(query, conn)\n",
    "res=res[res['ndc'].isin([str(v)[2:] for v in mat.columns.tolist()])]\n",
    "drugs_labels=res.groupby('ndc').first()['drug'].tolist()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "query = f\"\"\"SELECT icd_code, long_title FROM d_icd_procedures\"\"\"\n",
    "res=sqlio.read_sql_query(query, conn)\n",
    "proc_labels=res[res['icd_code'].isin([str(v)[2:] for v in mat.columns.tolist()])]['long_title'].tolist()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "labels=drugs_labels+proc_labels\n",
    "if len(labels) != len(mat.columns):\n",
    "    print(\"There is an error in the labels\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('mimic_data_labels.pkl', 'wb') as f:\n",
    "    pickle.dump(labels, f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Collect the diagnosis\n",
    "\n",
    "There are several diagnosis for each visit. We limit the number of diagnosis to 5 per visit. \n",
    "\n",
    "The diasgnosis are ordered by \"importance\" (see https://mimic.mit.edu/docs/iv/modules/hosp/diagnoses_icd/). \n",
    "1 is the most important."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "query = f\"\"\"SELECT procevent.hadm_id, d.icd_code, seq_num as importance, long_title AS label FROM diagnoses_icd as d\n",
    "                                            JOIN d_icd_diagnoses AS icd ON icd.icd_code=d.icd_code\n",
    "                                            JOIN ({query_index_events}) AS procevent ON procevent.hadm_id=d.hadm_id WHERE seq_num<=5 ORDER BY procevent.hadm_id, seq_num\"\"\"\n",
    "res=sqlio.read_sql_query(query, conn)\n",
    "with open('mimic_diagnosis.pkl', 'wb') as f:\n",
    "    pickle.dump(res, f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Connection closure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Close the connection to the database"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# closing database connection.\n",
    "if conn:\n",
    "    conn.close()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.8.10 64-bit",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "vscode": {
   "interpreter": {
    "hash": "916dbcbb3f70747c44a77c7bcd40155683ae19c65e1c03b4aa3499c5328201f1"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
