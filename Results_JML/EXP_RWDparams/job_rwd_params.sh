#!/bin/bash

exp="EXP_RWD_$(date +%F)_$(date +%s)"

mkdir -p "$exp/"
file="$exp/results.csv"

echo -e "dataset,it,R,Tw,loss,model,normalization,sparsity,pheno_succession,error_Ph,error_W_train,error_X_train,time,error_W_test,error_X_test" >> $file

#### default parameters
R=12
loss='Bernoulli'
normalization='True'
phenotypesuccession=0.25
sparsity=0.25
batchsize=50
epochs=300

for R in 12 #12 4 36
do
for model in "fastswotted" #"fastswotted" "swotted"
do
for dataset_name in "eshop" #"eshop" "bike" "mimic_data_tuple"
do
    dataset="../data/${dataset_name}_size6.pkl"
    for Tw in 3 # 5 4 3 2 1
    do
    	for phenotypesuccession in 3 # 0.0 0.25 0.5 1 1.5 2 3 #0.0 0.125 0.25 0.5 0.75 1
    	do
    	for sparsity in 2 3 # 0.0 0.25 0.5 1 1.5 2 3 #0.0 0.125 0.25 0.5 0.75 1
    	do
        for it in {1..5}
        do
            cmd="../competitors/run_$model.py -it $it -l $loss -p $dataset -r $R -tw $Tw -b $batchsize -e $epochs -sp $sparsity -ps $phenotypesuccession -mi -dir ."
            if [ "$normalization" = "True" ]; then
                cmd="${cmd} -nr"
            fi
            res=$(python3 $cmd)
            echo -e "${dataset_name},$it,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession,$res" >> $file
        done
        done
        done
    done
done
done
done
