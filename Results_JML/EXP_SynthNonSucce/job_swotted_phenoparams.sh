#!/bin/bash
exp="EXP_$(date +%F)_$(date +%s)"

mkdir -p "$exp/"
file="$exp/results.csv"

cp $0 $exp

echo -e "it,R_hidden,Tw_hidden,K,N,T,R,Tw,loss,model,normalization,sparsity,pheno_nonsuccession,error_Ph,error_W_train,error_X_train,time,error_W_test,error_X_test" >> $file

# default synthetic dataset parameters
K=200
N=20
T=20
Rhidden=4
Twhidden=3 # 1 is mandatory for comparison with LogPar, CNTF and SWIFT in order to be able to compute the FIT

# default models' parameters
R=4
Tw=$Twhidden
loss='Bernoulli'
normalization='True'
phenotypesuccession=0.25
sparsity=0.25
batchsize=50
epochs=300

for N in 20
do
for T in 10
do
for R in 10 # mandatory for this experiment
do
Rhidden=$R
for it in {1..10}
do  
    dataset="$exp/data_${it}_sw.pickle"
    python3 experiments_gen_data.py -k $K -n $N -t $T -r $Rhidden -tw $Twhidden -tr -sw -ph -p $dataset
    
    for model in swotted fastswotted # "swotted" "fastswotted"
    do 
    	for loss in 'Bernoulli' #'Poisson' 'Frobenius'
    	do
    	for phenotypesuccession in 0.0 0.5 1 2 # 50 #0.0 0.125 0.25 0.5 0.75 1
    	do
    	for sparsity in 0.0 2 8 16 32 # 0.0 0.5 1 #0.0 0.125 0.25 0.5 0.75 1
    	do
    	for normalization in 'True' # 'True' 'False'
    	do
            echo "running $model..." 
            cmd="../competitors/run_$model.py -it $it -l $loss -p $dataset -r $R -tw $Tw -b $batchsize -e $epochs -sp $sparsity -ps $phenotypesuccession"
            if [ "$normalization" = "True" ]; then
                cmd="${cmd} -nr"
            fi
            res=$(python3 $cmd)
            echo -e "$it,$Rhidden,$Twhidden,$K,$N,$T,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession, $res" >> $file
        done
        done
        done
        done

    done

done
done
done
done

