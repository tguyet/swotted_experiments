#!/bin/bash

exp="EXP_RWD_$(date +%F)_$(date +%s)"

mkdir -p "$exp/"
file="$exp/results.csv"

cp $0 $exp

echo -e "dataset,it,R,Tw,loss,model,normalization,sparsity,pheno_succession,error_Ph,error_W_train,error_X_train,time,error_W_test,error_X_test" >> $file

#### default parameters
R=12
loss='Bernoulli'
normalization='True'
phenotypesuccession=0.25
sparsity=0.25
batchsize=50
epochs=300

for dataset_name in bike #eshop bike mimic_data_tuple
do
    #dataset="../data/${dataset_name}_size6.pkl"
    dataset="../data/${dataset_name}.pkl"
    for it in {1..10}
    do
    for R in 4 36 12 #4 36 12
    do
    for model in swotted CNTF #SWIFT LogPar #fastswotted swotted CNTF SWIFT LogPar
    do
        if [ "$model" = "swotted"  ] || [ "$model" = "fastswotted"  ] ; then
            for Tw in 5 3 1
            do
                for phenotypesuccession in 0.5 # 0.0 0.25 0.5 1 1.5 2 3 #0.0 0.125 0.25 0.5 0.75 1
                do
                for sparsity in 2 #0.0 0.5 1 1.5 # 0.0 0.25 0.5 1 1.5 2 3 #0.0 0.125 0.25 0.5 0.75 1
                do
                    cmd="../competitors/run_$model.py -it $it -l $loss -p $dataset -r $R -tw $Tw -b $batchsize -e $epochs -sp $sparsity -ps $phenotypesuccession -mi -dir $exp"
                    if [ "$normalization" = "True" ]; then
                        cmd="${cmd} -nr"
                    fi
                    res=$(python3 $cmd)
                    echo -e "${dataset_name},$it,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession,$res" >> $file

                    mv $exp/model.pkl $exp/model_${model}_${R}_${Tw}_${it}.pkl
                done
                done
            done
            
        elif [ "$model" = "CNTF" ]; then
            Tw=1
            echo "running CNTF..." 
            res=$(python3 ../competitors/cntf/run_cntf.py -it $it -e $epochs -p $dataset -r $R -dir $exp)
            mv $exp/model.pkl $exp/model_${model}_${R}_${Tw}_${it}.pkl 

            echo -e "$it,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession, $res" >> $file
        elif [ "$model" = "SWIFT" ]; then
            Tw=1
            echo "running SWIFT ..." 
            res=$(python3 ../competitors/swift/run_swift.py -it $it -e 100 -p $dataset -r $R --name $exp)

            echo -e "$it,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession, $res" >> $file
        elif  [ "$model" = "LogPar" ]; then
            Tw=1
            echo "running LogPar ..." 
            res=$(python3 ../competitors/LogPar/run_logpar.py -it $it --data_path $dataset --rank $R --epochs $epochs --lr 0.001 --name $exp)

            echo -e "$it,$R,$Tw,$loss,$model,$normalization,$sparsity,$phenotypesuccession, $res" >> $file
        fi
                
    done
    done
    done
done


