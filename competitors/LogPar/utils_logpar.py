"""
Useful function for Logpar usage

@author: Hana Sebia
"""


import torch
import numpy as np
from munkres import Munkres


def truncate_patient_stays(X, days=10):
    """
    Params:
    - X : a list of K matrices of size N x T, where N is the number of drugs and procedures and T is the length of a patient stay
    - days : the considered number of days of patient stays
    """
    result = [torch.zeros([X[0].shape[0], days]) for _ in range(len(X))]

    for k in range(len(X)):
        for n in range(X[k].shape[0]):
            for t in range(days):
                if t < X[k].shape[1]:
                    result[k][n][t] = X[k][n][t]
    return result


def tensor_to_logpar(X):
    """
    Params:
    - X : a list of K matrices of size N x T, where N is the number of drugs and procedures and T is the length of a patient stay
    """
    data = []

    if isinstance(X,list):
        Y= [torch.clip(Xi, 0.0, 1.0) for Xi in X]
    else:
        Y= torch.clip(X, 0.0, 1.0)

    for k in range(len(Y)):
        dict_pt = {}
        dict_pt["pid"] = k
        train_list = []
        for n in range(Y[k].shape[0]):
            for t in range(Y[k].shape[1]):
                train_list.append([t, n, Y[k][n][t]])
        dict_pt["train"] = np.array(train_list)
        dict_pt["validation"] = np.array([train_list[i] for i in range(0)])
        dict_pt["test"] = np.array([train_list[i] for i in range(0)])
        dict_pt["times"] = [0 for _ in range(Y[k].shape[1])]
        dict_pt["deltas"] = [1 for _ in range(Y[k].shape[1])]
        dict_pt["label"] = None
        data.append(dict_pt)

    return data


def reorderPhenotypes_logPar(gen_pheno, resulting_pheno, pathways):
    """
    Params :
    - gen_pheno : a 2D tensor containing the generated phenotypes
    - resulting_pheno : a 2D tensor containing the resulting phenotypes
    - pathways : a 3D tensor representing patients pathways
    Returns :
    - reordered_pheno : a 2D tensor containing the reordered resulting phenotypes
    - reordered_pathways : a 3D tensor representing reordered patients pathways
    """
    if gen_pheno.shape != resulting_pheno.shape:
        # print("generated phenotypes : ", gen_pheno.shape)
        # print("resulting phenotypes : ", resulting_pheno.shape)
        raise ValueError(
            "The generated phenotypes and computed phenotypes doesn't have the same shape"
        )

    dic = np.zeros(
        (gen_pheno.shape[0], resulting_pheno.shape[0])
    )  # construct a cost matrix

    for i in range(gen_pheno.shape[0]):
        for j in range(resulting_pheno.shape[0]):
            dic[i][j] = torch.norm((gen_pheno[i] - resulting_pheno[j]), p="fro").item()

    m = Munkres()  # Use of Hungarian Algorithm to find phenotypes correspondances
    indexes = m.compute(dic)

    # Reorder phenotypes
    reordered_pheno = resulting_pheno.clone()
    for row, column in indexes:
        reordered_pheno[row] = resulting_pheno[column]

    # Reorder pathways
    reordered_pathways = [pathways[i].clone() for i in range(len(pathways))]
    for i in range(len(pathways)):
        for row, column in indexes:
            reordered_pathways[i][column] = pathways[i][row]

    return reordered_pheno, reordered_pathways
