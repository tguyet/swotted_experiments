import argparse
import pickle
import torch


## required to simply execute from the experiments repository
import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../..")))

from experiments.utils import success_rate
from utils_logpar import (
    tensor_to_logpar,
    reorderPhenotypes_logPar,
    truncate_patient_stays,
)
import time


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--name", "-n", type=str, default="EXP", help="The name of the experiment"
    )
    parser.add_argument(
        "--iteration",
        "-it",
        type=int,
        default=0,
        help="the number of the iteration of the experiment.",
    )
    parser.add_argument(
        "--data_path",
        "-d",
        type=str,
        default="./demo_data.pkl",
        help="The path of input data.",
    )

    parser.add_argument(
        "--uniqueness",
        "-u",
        type=float,
        default=1e-3,
        help="Weighting for the uniqueness regularization.",
    )
    parser.add_argument(
        "--rank",
        "-r",
        type=int,
        default=4,
        help="Target rank of the PARAFAC2 factorization.",
    )
    parser.add_argument(
        "--alpha",
        type=float,
        default=2,
        help="Maximam infinity norm allowed for the factor " "matrices.",
    )
    parser.add_argument(
        "--gamma",
        type=float,
        default=1,
        help="Shape parameter for the sigmoid function.",
    )
    parser.add_argument(
        "--lr", type=float, default=1e-3, help="Learning rate of the optimizers."
    )
    parser.add_argument(
        "--wd", type=float, default=0, help="Weight decay for the optimizers."
    )
    parser.add_argument("--batch_size", type=int, default=50)
    parser.add_argument("--epochs", type=int, default=500)
    parser.add_argument(
        "--smooth",
        type=float,
        default=1,
        help="Weighting for the smoothness regularization.",
    )

    return parser.parse_args()


if __name__ == "__main__":
    args = get_arguments()

    dir = "../competitors/LogPar/" + args.name + "/" + str(args.iteration) + "/"

    # clean the data
    try:
        os.remove(dir + "model.pt")
        os.remove(dir + "config.json")
    except FileNotFoundError:
        pass

    os.makedirs(dir, exist_ok=True)

    # Load data
    W_, Ph_, X, params = pickle.load(open(args.data_path, "rb"))
    data = tensor_to_logpar(X)
    pickle.dump(data, open(dir + "test_data.pkl", "wb"))

    # run LogPar
    run_logpar = (
        f"python3 ../competitors/LogPar/main.py --data_path "
        + dir
        + f"test_data.pkl --uniqueness {args.uniqueness} --rank {args.rank} --alpha {args.alpha} --gamma {args.gamma} --lr {args.lr} --batch_size {args.batch_size} --epochs {args.epochs} --smooth {args.smooth} --out "
        + dir
    )

    try:
        start = time.time()
        os.system(run_logpar)
        duration = time.time() - start
    except:
        print("NA,NA,NA,0.0")
        exit(0)

    train_idx, test_idx = pickle.load(open(dir + "train_test_indexes.pkl", "rb"))

    # Evaluate the model
    model = torch.load(dir + "model.pt")
    Y_train = torch.transpose(
        torch.einsum("ptr,pr,fr->ptf", model["U"], model["S"], model["V"]), 1, 2
    )

    FIT_X_train = success_rate(
        [X[p] for p in train_idx], [torch.clamp(Y, 0, 1) for Y in Y_train]
    )

    if Ph_ == None:
        res_str = "NA,NA,"
    else:
        try:
            Ph, W = reorderPhenotypes_logPar(
                Ph_,
                torch.transpose(model["V"], 0, 1),
                torch.transpose(model["U"], 1, 2),
            )
            FITP = success_rate(Ph_, Ph)
            FITW = success_rate([W_[p] for p in train_idx], W)
            res_str = str(FITP) + "," + str(FITW) + ","
        except ValueError:
            res_str = "NA,NA,"

    res_str += str(FIT_X_train) + "," + str(duration) + ","

    # Evaluate the projection
    projector = torch.load(dir + "projector.pt")
    Y_test = torch.transpose(
        torch.einsum("ptr,pr,fr->ptf", projector["U"], projector["S"], projector["V"]),
        1,
        2,
    )
    FIT_X_test = success_rate(
        [X[p] for p in test_idx], [torch.clamp(Y, 0, 1) for Y in Y_test]
    )

    if Ph_ == None:
        res_str += "NA," + str(FIT_X_test)
    else:
        W = projector["U"].transpose(2, 1)
        FIT_W_test = success_rate([W_[p] for p in test_idx], W)

        res_str += str(FIT_W_test) + "," + str(FIT_X_test)

    print(res_str)
