import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
import time

from torch.utils.data import DataLoader
from swotted.swotted import swottedModule, swottedTrainer, SlidingWindow
from swotted.swotted.utils import Subset
from swotted.swotted.loss_metrics import *

from experiments.utils import success_rate

import numpy as np
from omegaconf import OmegaConf

from sklearn.model_selection import train_test_split

import argparse
import pickle
from lightning.pytorch.callbacks import TQDMProgressBar


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-it", "--iteration", type=int, help="specify the number of iterations"
    )
    parser.add_argument(
        "-dir", "--directory", type=str, help="specify the directory to store the model"
    )
    parser.add_argument("-l", "--loss", help="[Bernoulli, Poisson, Frobenius]")
    parser.add_argument(
        "-p", "--datasetfile", help="specify the filename of input data"
    )
    parser.add_argument(
        "-r", "--phenotypes", type=int, help="specify the number of phenotypes"
    )
    parser.add_argument(
        "-tw",
        "--temporal_window",
        type=int,
        help="specify the length of the temporal window",
    )
    parser.add_argument("-b", "--batch", type=int, help="specify the batch size")
    parser.add_argument("-e", "--epochs", type=int, help="specify the number of epochs")

    parser.add_argument("-sp", "--sparsity", type=float, help="alpha parameter")
    parser.add_argument("-ps", "--pheno_succession", type=float, help="beta parameter")

    parser.add_argument("-nr", "--normalization", action="store_true")

    parser.add_argument("-mi", "--mimic", action="store_true")
    parser.add_argument("--noshuffle", action="store_true", help="Do not shuffle the examples to create test/train datasets")

    return parser.parse_args()


if __name__ == "__main__":
    args = get_arguments()

    # Load synthetic data
    W_, Ph_, X, params = pickle.load(open(args.datasetfile, "rb"))

    if args.mimic:
        data = []
        for x in X:
            if x.shape[1] >= args.temporal_window:
                data.append(x)

        X = data
        W_ = [None] * len(data)

    N = X[0].shape[0]  # nb of features

    # Split the data into train/test dataset
    X_train, X_test, W_train, W_test = train_test_split(
        X, W_, train_size=0.7, test_size=0.3, shuffle=not args.noshuffle
    )

    # launch cntf
    R = args.phenotypes  #: number of phenotypes
    Tw = args.temporal_window  #: length of time's window

    params = {}
    params["model"] = {}
    params["model"]["non_succession"] = args.pheno_succession
    params["model"]["sparsity"] = args.sparsity
    params["model"]["rank"] = R
    params["model"]["twl"] = Tw
    params["model"]["N"] = N
    params["model"]["metric"] = args.loss
    params["training"] = {}
    params["training"]["batch_size"] = args.batch
    params["training"]["nepochs"] = args.epochs
    params["training"]["lr"] = 1e-2
    params["predict"] = {}
    params["predict"]["nepochs"] = 100
    params["predict"]["lr"] = 1e-2

    config = OmegaConf.create(params)

    # define the model
    swotted = swottedModule(config)

    swotted.normalization = args.normalization

    train_loader = DataLoader(
        Subset(X_train, np.arange(len(X_train))),
        batch_size=params["training"]["batch_size"],
        shuffle=False,
        collate_fn=lambda x: x,
    )

    # train the model (hint: here are some helpful Trainer arguments for rapid idea iteration)
    trainer = swottedTrainer(
        callbacks=[TQDMProgressBar(refresh_rate=0)],
        fast_dev_run=False,
        max_epochs=params["training"]["nepochs"],
    )

    start = time.time()
    trainer.fit(model=swotted, train_dataloaders=train_loader)
    duration = time.time() - start

    # reconstruction
    Y = [
        torch.clamp(swotted.model.reconstruct(wk, swotted.Ph), 0, 1)
        for wk in swotted.Wk
    ]

    error_X_train = success_rate(X_train, Y)

    try:
        pickle.dump(swotted, open(args.directory + "/model.pkl", "wb"))
        pickle.dump([X_train, Y], open(args.directory + "/train_reconstruction.pkl", "wb"))
    except:
        pass

    if Ph_ == None:
        res_str = "NA,NA,"
    else:
        try:
            if Ph_.dim() == 2:
                Ph_ = Ph_.unsqueeze(2)
            reorderedPh, reorderedW = swotted.reorderPhenotypes(Ph_)
            error_Ph = success_rate(Ph_, reorderedPh.clip(0, 1))

            error_W_train = success_rate(W_train, [w.clip(0, 1) for w in reorderedW])
            res_str = str(error_Ph) + "," + str(error_W_train) + ","
        except ValueError:
            res_str = "NA,NA,"

    res_str += str(error_X_train) + "," + str(duration) + ","

    # Lunch the projection on the test dataset
    Wk_test = swotted(X_test)

    if args.mimic:
        res_str += "NA,"
    else:
        error_W_test = success_rate(W_test, Wk_test)
        res_str += str(error_W_test) + ","

    Y = [torch.clamp(swotted.model.reconstruct(wk, swotted.Ph), 0, 1) for wk in Wk_test]
    error_X_test = success_rate(X_test, Y)
    res_str += str(error_X_test)
    try:
        pickle.dump([X_test, Y], open(args.directory + "/test_reconstruction.pkl", "wb"))
    except:
        pass


    print(res_str)
