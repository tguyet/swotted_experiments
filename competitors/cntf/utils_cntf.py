"""
Useful function for CNTF usage

@author: Hana Sebia
"""

import torch
import numpy as np
from munkres import Munkres


def tensor_to_cntf(X):
    """
        Params:
        - X : a list of K matrices of size N x T, where N is the number of drugs and procedures and T is the length of a patient stay 
    """

    listDico = []
    listID = []
    for p in range(len(X)):
        dictionary = {}
        subs = [[],[],[]]
        vals = []
        for n in range(X[p].shape[0]):
            for t in range(X[p].shape[1]):
                if X[p][n][t] != 0 :
                    subs[0].append(t)
                    subs[1].append(n)
                    subs[2].append(0)
                    vals.append(X[p][n][t])
        dictionary["subs"] = torch.tensor(subs, dtype=torch.int64)
        dictionary["vals"] = torch.tensor(vals).to(dtype=torch.int64)
        dictionary["size"] = torch.Size([X[p].shape[1], X[p].shape[0], 1], dtype=torch.int64)
        dictionary["hadm_id"] = p
        dictionary["dx_vector"] = torch.tensor([])
        dictionary["rx_vector"] = torch.tensor([])
        listDico.append(dictionary)
        listID.append([p, X[p].shape[1],0])
    return listDico,listID


def data_reconstruction_cntf(pathways, phenotypes):
    """
        Params:
        - pathways : a list of tensors representing patient pathways of size K x T x R
        - phenotypes : a matrix of phenotypes of size N x R
        Returns:
        - X : a tensor of size K x N x T, where K is the number of patients, T is the lengh of patient stays and N is the number of drugs

    """
    return [torch.transpose(torch.tensordot(torch.transpose(pathways[i],0,1), phenotypes, dims=1),0,1) for i in range(len(pathways))]


def reorderPhenotypes_cntf(gen_pheno, resulting_pheno, pathways):
    """
        Params : 
        - gen_pheno : a 2D tensor containing the generated phenotypes
        - resulting_pheno : a 2D tensor containing the resulting phenotypes
        - pathways : a 3D tensor representing patients pathways
        Returns : 
        - reordered_pheno : a 2D tensor containing the reordered resulting phenotypes
        - reordered_pathways : a 3D tensor representing reordered patients pathways
    """
    if gen_pheno.shape != resulting_pheno.shape:
        raise ValueError("The generated phenotypes and computed phenotypes doesn't have the same shape")

    dic = np.zeros((gen_pheno.shape[0], resulting_pheno.shape[0]))   # construct a cost matrix
    
    for i in range(gen_pheno.shape[0]):
        for j in range(resulting_pheno.shape[0]):
            dic[i][j] = torch.norm((gen_pheno[i]-resulting_pheno[j]), p='fro').item()
    
    m = Munkres() # Use of Hungarian Algorithm to find phenotypes correspondances
    indexes = m.compute(dic)

    # Reorder phenotypes
    reordered_pheno = resulting_pheno.clone()
    for row, column in indexes:
        reordered_pheno[row] = resulting_pheno[column]
        
    # Reorder pathways
    reordered_pathways = [pathways[i].clone() for i in range(len(pathways))]
    for i in range(len(pathways)):
        for row, column in indexes:
            reordered_pathways[i][column] = pathways[i][row]
                    
    
    return reordered_pheno, reordered_pathways
