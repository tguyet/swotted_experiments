import argparse
import pickle
import torch


## required to simply execute from the experiments repository
import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../..")))

from models import TemporalDependency
from temporal_phenotyping import TemporalPhenotyping

from sklearn.model_selection import train_test_split
from cntf import launch, SyntheticDataset
from utils_cntf import tensor_to_cntf, data_reconstruction_cntf, reorderPhenotypes_cntf
from experiments.utils import success_rate
import time
import warnings


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-it", "--iteration", type=int, help="specify the number of iterations"
    )
    parser.add_argument(
        "-a", "--alpha", help="alpha parameter (default 1.0)", default=1
    )
    parser.add_argument("-hf", "--hitf", help="HITF parameter (default 0)", default=0)
    parser.add_argument("-b", "--beta", help="beta parameter (default 0)", default=0)
    parser.add_argument(
        "-p", "--datasetfile", help="specify the filename of input data"
    )
    parser.add_argument(
        "-r",
        "--phenotypes",
        type=int,
        help="specify the number of phenotypes (default 4)",
        default=4,
    )
    parser.add_argument(
        "-e",
        "--epochs",
        type=int,
        help="specify the number of epochs (default 100)",
        default=400,
    )
    parser.add_argument(
        "-dir",
        "--directory",
        type=str,
        help="specify the directrory to store the model",
    )

    return parser.parse_args()


if __name__ == "__main__":
    warnings.filterwarnings("ignore")

    args = get_arguments()

    # Load data
    W_, Ph_, X, params = pickle.load(open(args.datasetfile, "rb"))

    # adapt the structure to CNTF
    dic = {}
    dic["listDico"], dic["listID"] = tensor_to_cntf(X)
    device = torch.device("cpu")
    dataset = SyntheticDataset(dic["listDico"], dic["listID"], device)

    # launch cntf
    R = args.phenotypes
    alphaCNTF = args.alpha
    alphaHITF = args.hitf
    beta = args.beta
    nepoch = args.epochs
    temporalModel = TemporalDependency
    temporal_model_params = {"nlayers": 2, "nhidden": 200, "dropout": 0}

    # Split the data into train/test
    train_idx, test_idx, *_ = train_test_split(
        list(range(len(dataset.labels))), dataset.labels, train_size=0.7, test_size=0.3
    )

    # Initialize the cntf model
    model = TemporalPhenotyping(
        exp_name="",
        rank=R,
        alpha_CNTF=alphaCNTF,
        alpha_HITF=alphaHITF,
        beta=beta,
        alpha_Sparsity=0,
        temporal_model=temporalModel,
        temporal_model_params=temporal_model_params,
        device=device,
    )

    # Launch the training
    start = time.time()

    model.fit(
        dataset=dataset,
        train_idx=train_idx,
        batch_size=50,
        random_seed=False,
        pretrain_epochs=10,
        n_epochs=100,
    )

    duration = time.time() - start

    # Get the results
    dic["Um"], dic["Ul"], dic["Ud"], dic["Ws_train"] = (
        torch.transpose(model.Ul.data, 0, 1),
        model.Um.data,
        model.Ud.data,
        [torch.transpose(model.Ws[i], 0, 1) for i in range(len(model.Ws))],
    )

    try:
        pickle.dump(dic["Um"], open(args.directory + "/model.pkl", "wb"))
    except:
        pass

    Y_train = data_reconstruction_cntf(
        [dic["Ws_train"][p] for p in train_idx], dic["Um"]
    )
    Y_train = [torch.clamp(Y, 0, 1) for Y in Y_train]

    FIT_X_train = success_rate([X[p] for p in train_idx], Y_train)

    if Ph_ == None:
        res_str = "NA,NA,"
    else:
        try:
            Ph, W = reorderPhenotypes_cntf(Ph_, dic["Um"], dic["Ws_train"])
            FITP = success_rate(Ph_, Ph)
            FITW = success_rate([W_[p] for p in train_idx], W)
            res_str = str(FITP) + "," + str(FITW) + ","
        except:
            res_str = "NA,NA,"

    res_str += str(FIT_X_train) + "," + str(duration) + ","

    # Projection
    model.project(dataset=dataset, test_idx=test_idx, batch_size=50, proj_epochs=30)

    dic["Ws_test"] = [torch.transpose(model.Ws[i], 0, 1) for i in range(len(model.Ws))]

    Y_test = data_reconstruction_cntf([dic["Ws_test"][p] for p in test_idx], dic["Um"])
    Y_test = [torch.clamp(Y, 0, 1) for Y in Y_test]

    FIT_X_test = success_rate([X[p] for p in test_idx], Y_test)

    if Ph_ == None:
        res_str += "NA," + str(FIT_X_test)
    else:
        FIT_W_test = success_rate([W_[p] for p in test_idx], dic["Ws_test"])

        res_str += str(FIT_W_test) + "," + str(FIT_X_test)

    print(res_str)
