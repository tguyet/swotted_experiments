#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 14:39:42 2021

@author: tguyet
"""

from torch.utils.data import Dataset
import pickle
import numpy as np

import torch
from sklearn.model_selection import train_test_split

from temporal_phenotyping import TemporalPhenotyping
from models import TemporalDependency, MortalitySupervised



class SyntheticDataset(Dataset):
    """
    author: Thomas Guyet, Inria
    """
    def __init__(self,listDico,listID, device):

        self.hadm_id_list, self.los_days, labels = zip(*listID)
        self.labels = [1 if label == 1 else 0 for label in labels]
        # load all
        self.spt = [torch.sparse.FloatTensor(hadm['subs'], hadm['vals'], hadm['size']).to(device) for hadm in listDico]
        self.rx_vectors = [hadm['rx_vector'].to(device) for hadm in listDico]
        self.dx_vectors = [hadm['dx_vector'].to(device) for hadm in listDico]

    def __len__(self):
        return len(self.hadm_id_list)

    def __getitem__(self, idx):
        return self.spt[idx], self.rx_vectors[idx], self.dx_vectors[idx], self.labels[idx], idx


def launch(listDico, listID, R, randomState , alphaCNTF, alphaHITF, beta, temporalModel, n_epochs=100):
    
    device = torch.device('cpu')
    dataset = SyntheticDataset(listDico,listID, device)


    temporal_model_params = {'nlayers': 2, 'nhidden': 200, 'dropout': 0}

    train_idx, test_idx, *_ = train_test_split(list(range(len(dataset.labels))),
                                               dataset.labels,
                                               train_size=0.7,
                                               test_size=0.3,
                                               random_state=randomState)
    model = TemporalPhenotyping(exp_name='',
                                rank=R,alpha_CNTF = alphaCNTF , alpha_HITF = alphaHITF ,beta = beta,
                                alpha_Sparsity = 0, temporal_model = temporalModel,
                                temporal_model_params = temporal_model_params,
                                device=device)
    

    

    #reduction à 10 epoch à la place de 50
    model.fit(dataset=dataset, train_idx=train_idx, batch_size=50, random_seed=randomState, pretrain_epochs=10, n_epochs=n_epochs)
    model.project(dataset=dataset, test_idx=test_idx, batch_size=50, proj_epochs=30)
    

    return torch.transpose(model.Ul.data,0,1),model.Um.data,model.Ud.data,[torch.transpose(model.Ws[i],0,1) for i in range(len(model.Ws))]




def resultLR(listDico, listID, R,U_init_lr,Ws_init_lr):
    random_state = 42
    device = torch.device('cpu')
    dataset = SyntheticDataset(listDico,listID, device)


    temporal_model_params = {'nlayers': 2, 'nhidden': 200, 'dropout': 0}

    train_idx, test_idx, *_ = train_test_split(list(range(len(dataset.labels))),
                                               dataset.labels,
                                               train_size=0.7,
                                               test_size=0.3,
                                               random_state=random_state)
    model = TemporalPhenotyping(exp_name='',
                                rank=R,alpha_CNTF = 1 , alpha_HITF = 0 ,beta = 0,
                                alpha_Sparsity = 0, temporal_model = TemporalDependency,
                                temporal_model_params = temporal_model_params,
                                U_init_lr = U_init_lr,
                                Ws_init_lr = Ws_init_lr,
                                device=device)
    

    

    #reduction à 10 epoch à la place de 50
    model.fit(dataset=dataset, train_idx=train_idx, batch_size=50, random_seed=random_state, pretrain_epochs=10, n_epochs=100)
    model.project(dataset=dataset, test_idx=test_idx, batch_size=50, proj_epochs=30)
    
    return model.lossValues


