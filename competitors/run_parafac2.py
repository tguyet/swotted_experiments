import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

os.environ['PYDEVD_WARN_EVALUATION_TIMEOUT']="10"

import time
import torch

import tensorly as tl
from tensorly.decomposition import parafac2

from experiments.utils import success_rate

import numpy as np
from omegaconf import OmegaConf

from sklearn.model_selection import train_test_split

import argparse
import pickle
from munkres import Munkres

def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-it", "--iteration", type=int, help="specify the number of iterations"
    )
    parser.add_argument(
        "-dir", "--directory", type=str, help="specify the directory to store the model"
    )
    parser.add_argument("-l", "--loss", help="[Bernoulli, Poisson, Frobenius]")
    parser.add_argument(
        "-p", "--datasetfile", help="specify the filename of input data"
    )
    parser.add_argument(
        "-r", "--phenotypes", type=int, help="specify the number of phenotypes"
    )
    parser.add_argument(
        "-e", "--epochs", type=int, help="number of epochs", default=100
    )

    parser.add_argument("-mi", "--mimic", action="store_true")
    parser.add_argument("--noshuffle", action="store_true", help="Do not shuffle the examples to create test/train datasets")

    return parser.parse_args()

def reorderPhenotypes(Ph, Ph_):
    """
    Ph: extracted phenotypes to reorder
    Ph_: phenotypes according to which reorder Ph"""
    dic = np.zeros(
        (Ph_.shape[0], Ph.shape[0])
    )  # construct a cost matrix
    for i in range(Ph_.shape[0]):
        for j in range(Ph.shape[0]):
            dic[i][j] = np.sum((Ph_[i] - Ph[j])**2)

    m = Munkres()  # Use of Hungarian Algorithm to find phenotypes correspondances
    indexes = m.compute(dic)

    # Reorder phenotypes
    reordered_pheno = Ph.copy()
    for row, column in indexes:
        reordered_pheno[row] = Ph[column]

    return reordered_pheno

if __name__ == "__main__":
    args = get_arguments()

    # Load synthetic data
    W_, Ph_, X, params = pickle.load(open(args.datasetfile, "rb"))

    X=X[:100]
    W_=W_[:100]

    if args.mimic:
        data = []
        for x in X:
            if x.shape[1] >= args.temporal_window:
                data.append(x)

        X = data
        W_ = [None] * len(data)

    N = X[0].shape[0]  # nb of features
    R = args.phenotypes  #: number of phenotypes

    # Split the data into train/test dataset
    X_pretrain, X_test, W_train, W_test = train_test_split(
        X, W_, train_size=0.7, test_size=0.3, shuffle=not args.noshuffle
    )

    X_train = [x.detach().numpy().transpose() for x in X_pretrain if x.shape[1] >=R]
    if len(X_train)!=len(X_pretrain):
        Warning.warn(f"PARAFAC2 limitation: R must be lower that the tensors dimensions: removed shortests sequences from {len(X_train)} to {len(X_pretrain)} examples.")

    # launch decomposition
    start = time.time()
    trial_decomposition = parafac2(X_train, R, return_errors=False, tol=1e-8, n_iter_max=args.epochs, nn_modes='all')
    duration = time.time() - start

    # reconstruction
    Y = tl.parafac2_tensor.parafac2_to_slices(trial_decomposition)
    
    error_X_train = success_rate([torch.tensor(x) for x in X_train], [torch.tensor(x) for x in Y])

    if Ph_ == None:
        res_str = "NA,NA,"
    else:
        est_A, est_B, Ph = tl.parafac2_tensor.apply_parafac2_projections(trial_decomposition)[1]
        try:
            if Ph_.dim() == 2:
                Ph_=Ph_.detach().numpy()
                reorderedPh = reorderPhenotypes(np.transpose(Ph.clip(0, 1)), Ph_)
                error_Ph = success_rate(torch.tensor(Ph_), torch.tensor(reorderedPh))
                res_str = str(error_Ph) + ",NA,"
            else:
                res_str = "NA,NA,"
        except ValueError:
            res_str = "NA,NA,"

    res_str += str(error_X_train) + "," + str(duration) + ","

    res_str += "NA,NA"


    print(res_str)
