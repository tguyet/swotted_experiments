SWIFT implementation
--------------------

Original code: http://kejing.me/publications/2021_AAAI_SWIFT_codes.zip 
	-> just copy the SWIFT directory in that directory

Use the TensorToolbox, Version 3.2.1 (see https://www.tensortoolbox.org/)
	-> just copy the tensor_toolbox directory in that directory

Inputs:
- dims.csv: a file with 3 lines: the size of each dimension of the tensor to decompose
- tensor.csv: a sparse description of the tensors. It is a CSV file with 4 columns, the three first columns gives the coordinates of a non-zero cell, and the fourth gives its value.
	Note that the first column has to have indices from 1 to n, but the two other columns must have indices between 0 and p-1.
	
Outputs:
- W.pkl the matrix of the care pathways
- P.pkl the phenotypes
