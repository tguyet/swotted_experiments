addpath(genpath('.'));
addpath(genpath('./SWIFT'));
addpath(genpath('./tensor_toolbox')); 

dims=dlmread('/tmp/dims.csv');

%Cost matrices : LxL matrices of random positive values, nul diag, values ~1
COST={};
%COST{1}= 1-abs(rand(dims(1),dims(1),1))/10; %Cost for Patients 
%COST{2}= 1-abs(rand(dims(2),dims(2),1))/10; %Cost for Diag
%COST{3}= 1-abs(rand(dims(3),dims(3),1))/10; %Cost for Time
% ensure the positivity of costs
%COST{1}=max(COST{1},0);
%COST{2}=max(COST{2},0);
%COST{3}=max(COST{3},0);
% put 0 on the diagonal
%COST{1}=COST{1} - COST{1}.*eye(dims(1));
%COST{2}=COST{2} - COST{2}.*eye(dims(2));
%COST{3}=COST{3} - COST{3}.*eye(dims(3));

%COST{1}=eye(dims(1));
%COST{2}=eye(dims(2));
%COST{3}=eye(dims(3));

COST{1}= ones(dims(1),dims(1)); %Cost for Patients 
COST{2}= ones(dims(2),dims(2)); %Cost for Diag
COST{3}= ones(dims(3),dims(3)); %Cost for Time
% put 0 on the diagonal
COST{1}=COST{1} - COST{1}.*eye(dims(1));
COST{2}=COST{2} - COST{2}.*eye(dims(2));
COST{3}=COST{3} - COST{3}.*eye(dims(3));

X=read_input_count('/tmp/','tensor.csv'); % X(i,j,k) matrix

[output]=SWIFT(X,R,COST{1},COST{2},COST{3},options,seed,0);


s = size(output{3});
W=zeros(s(1),s(2),s(2));
for i=1:s(2)
    W(:,:,i)=output.U{3};
end
W=pagemtimes(W, output.U{1}');

save /tmp/W.mat W
X=double(full(output));
save /tmp/X.mat X
P=output.U{2};
save /tmp/P.mat P

