import argparse
from email import parser
import pickle
import torch

import numpy as np
from munkres import Munkres
import scipy.io 
import random

## required to simply execute from the experiments repository
import sys
import os
import time

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))

# setup the command to Matlab
matlabcmd="matlab"

from experiments.utils import success_rate


def reorderPhenotypes(Ptarget, Porigin, Worigin = None):
	"""
		Params : 
		- Ptarget : a 2D tensor containing the phenotypes to which align the data
		- Porigin : a 2D tensor containing the phenotypes
		- Worigin : a 3D tensor representing patients pathways
		Returns : 
		- Preordered : a 2D tensor containing the reordered resulting phenotypes
		- Wreordered : a 3D tensor representing reordered patients pathways
	"""
	if Ptarget.shape != Porigin.shape:
		raise ValueError("The generated phenotypes and computed phenotypes doesn't have the same shape")

	costs = np.zeros((Ptarget.shape[0], Porigin.shape[0]))   # construct a cost matrix

	for i in range(Ptarget.shape[0]):
		for j in range(Porigin.shape[0]):
			costs[i][j] = torch.norm((Ptarget[i]-Porigin[j]), p='fro').item()
	
	m = Munkres() # Use of Hungarian Algorithm to find phenotypes correspondances
	indexes = m.compute(costs)

	# Reorder phenotypes
	if torch.is_tensor(Porigin):
		Preordered = Porigin.clone()
	else:
		Preordered = Porigin.copy()
	for row, column in indexes:
		Preordered[row] = Preordered[column]
		
	if not Worigin is None:
		# Reorder pathways
		if torch.is_tensor(Worigin[0]):
			Wreordered = [Worigin[i].clone() for i in range(len(Worigin))]
		else:
			Wreordered = [Worigin[i].copy() for i in range(len(Worigin))]
		for i in range(len(Worigin)):
			for row, column in indexes:
				Wreordered[i][column] = Worigin[i][row]
		return Preordered, Wreordered
	return Preordered


def get_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument("-n", "--name", type=str,
			default='EXP',
			help="specify the name of the experiment")
	parser.add_argument("-it", "--iteration", type=int,
			help="specify the number of iterations")
	parser.add_argument("-l", "--lbda",
			help="lambda parameter (default 1.0)", default=1)
	parser.add_argument("-g", "--gamma",
			help="gamma parameter (default 0)", default=0)
	parser.add_argument("-es", "--sinkhorn",
			help="Nb sinkhorn iterations (default 20)", default=20)
	parser.add_argument("-p", "--datasetfile",
			help="specify the filename of input data")
	parser.add_argument("-mt", "--maxtime",
			help="specify the maximum number of time instant in sequences (default 10)", default=10)
	parser.add_argument("-r", "--phenotypes", type=int,
			help="specify the number of phenotypes (default 4)", default=4)
	parser.add_argument("-e","--epochs", type=int,
			help="specify the number of epochs (default 100)", default=100)
	
	return parser.parse_args()


if __name__ == '__main__':

	args = get_arguments()

	# load the data
	W_, Ph_, X, params =pickle.load(open(args.datasetfile,"rb"))

	tmax=args.maxtime

	max_len=np.max([x.shape[1] for x in X])
	tmax=min(max_len,tmax) # determine the max length of the tensors (all tensors must have the same length)

	#number of features
	nbf=X[0].shape[0]

	#cut the pathways at the end to have at most tmax days + Transposition !!
	X = [np.transpose(d[:,0:tmax]) for d in X]
	#pad the pathways with zeros at the end to have exactly tmax days for all pathways
	X = torch.stack([torch.tensor(np.concatenate( (x, np.zeros((tmax-x.shape[0],nbf))), axis=0 )) for x in X])


	# clean the data
	try:
		os.remove("/tmp/dims.csv")
		os.remove("/tmp/tensor.csv")
		os.remove("/tmp/P.mat")
		os.remove("/tmp/W.mat")
		os.remove("/tmp/X.mat")
	except FileNotFoundError:
		pass

	with open('/tmp/dims.csv', 'w') as f:
		f.write(str(len(X))+'\n')
		f.write(str(nbf)+'\n')
		f.write(str(tmax)+'\n')

	#this line ensures that all dimensions will be represented (otherwise, there is an issue when loaded in matlab)
	X[0]=np.clip(X[0]+10e-16,0,1)

	#create a file representing the tensor
	with open('/tmp/tensor.csv', 'w') as f:
		for p in range(len(X)):
			for d in range(nbf):
				for t in range(tmax):
					if X[p][t,d]!=0:
						f.write(f'{p+1},{d},{t},{X[p][t,d]}\n')


	R=args.phenotypes
	lbda=args.lbda
	gamma=args.gamma
	nbiter=args.epochs
	nbsinkhorn=args.sinkhorn
	
	seed = random.randint(1, 10000)

	runswift=matlabcmd+f" -batch \"clc;R={R};options.lambda = {lbda};options.gamma = {gamma};options.Total_iteration={nbiter};options.sinkhorn_iter={nbsinkhorn};seed={seed};run('../competitors/swift/run_swift_withoutparams.m'); quit\""
	start = time.time()
	try:
		os.system(runswift)
	except:
		duration = time.time()- start
		print(f"NA,NA,NA,{duration},NA,NA")
		exit(0)

	duration = time.time()- start
	Xhat = scipy.io.loadmat('/tmp/X.mat')['X']
	Xhat=Xhat/np.max(Xhat) ##Change the scale ... otherwise, the results are too poor
	X = [np.transpose(d) for d in X]
	FITX = success_rate(X, [torch.tensor(x) for x in Xhat])

	if Ph_ == None:
		res_str = "NA,NA,"
	else :
		try:
			Ph = np.transpose(scipy.io.loadmat('/tmp/P.mat')['P'])
			W = scipy.io.loadmat('/tmp/W.mat')['W'].transpose( (1,2,0) ) #dimensions: id/R/T
			Ph, W = reorderPhenotypes(Ph_, Ph, W)
			FITP = success_rate(Ph_, torch.tensor(Ph))
			FITW = success_rate(W_, torch.tensor(W))
			res_str = str(FITP) + "," + str(FITW) +","
		except ValueError:
			res_str = "NA,NA,"
	
	res_str += str(FITX) + "," + str(duration)+ ",NA,NA"
	print(res_str)
	
