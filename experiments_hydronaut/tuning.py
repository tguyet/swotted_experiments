from hydronaut.experiment import Experiment
from hydronaut.decorator import with_hydronaut

import sys
sys.path.append(".")
sys.path.append("competitors/swotted")

from torch.utils.data import DataLoader
from swotted.swotted import swottedModule, swottedTrainer
from swotted.utils import Subset
from swotted.loss_metrics import *
from experiments.gen_data import gen_synthetic_data
from experiments_hydronaut.experiment import SWoTTeDExperiment

from omegaconf import open_dict

import numpy as np
import time
import matplotlib.pyplot as plt

import pickle

import warnings


class HyperparametersTuning(SWoTTeDExperiment):
    """Hydronaut experiment representing an experiment to fine tune a SWoTTeD model 
    on a datasets.

    To run this experiment from hyronaut script:
    ```bash
    export HYDRONAUT_CONFIG=experiments_hydronaut/experiment_tuning.yaml
    hydronaut-run```

    """

    def gen_phenotypes_image(self, filename="phenotypes.png"):
        """Create a PNG image to compare the extracted phenotypes.

        This function reorders the phenotypes.

        Parameters
        ==========
        filename: str
            name of the file to generate (default: 'phenotype.png')"""
        R=len(self.swotted.Ph)
        fig, axs = plt.subplots(R,1)
        for i in range(R):
            axs[i].imshow(
                self.swotted.Ph[i].detach().numpy(), cmap="gray", vmin=0, vmax=1, interpolation="nearest"
            )
            axs[i].set_ylabel("Features")
            axs[i].set_xlabel("Time")
        fig.savefig(filename)

    def __call__(self) -> float:
        """Implement the detail of one instance of the Hydronaut experiment.

        This experiment generates several artifacts:
        - `Ph.pkl`: the extracted phenotypes (the model)
        - ̀ phenotypes.png`: illustration of the phenotypes

        Returns
        --------
        float
            Test loss metric
        """

        params = self.config.experiment.params
        
        _, _, X, _ = pickle.load(open(params.data.filename, "rb"))
        if isinstance(X,list):
            nb_train = int(params.data.percent_train*len(X))
            nb_test = len(X)-nb_train

            #remove examples that do not have the correct length
            to_keep=[]
            for i in range(len(X)):
                if X[i].shape[1]>=params.model.twl:
                    to_keep.append(i)
            if len(to_keep)<len(X):
                warnings.warn(f"Removed {len(X)-len(to_keep)} examples with incompatible length")
                X=[X[i] for i in to_keep]

        else:
            nb_train = int(params.data.percent_train*X.shape[0])
            nb_test = X.shape[0]-nb_train

        with open_dict(params):
            params.model.N = X[0].shape[0]

        # define the model
        self.swotted = swottedModule(params)

        train_data_loader = DataLoader(
            Subset(X[: nb_train], np.arange(nb_train)),
            batch_size=params.training.batch_size,
            shuffle=False,
            collate_fn=lambda x: x,
        )
        test_data_loader = DataLoader(
            Subset(X[nb_train :], np.arange(len(X[nb_train :]))),
            batch_size=params.training.batch_size,
            shuffle=False,
            collate_fn=lambda x: x,
        )

        trainer = swottedTrainer(fast_dev_run=False, max_epochs=params.training.nepochs)
        before = time.time()
        trainer.fit(model=self.swotted, train_dataloaders=train_data_loader)
        duration = time.time() - before
        self.log_metric("training_time", duration)

        # save model
        pickle.dump(self.swotted.Ph, open("Ph.pkl", "wb"))
        self.log_artifact("Ph.pkl", "Ph_model")

        self.gen_phenotypes_image("phenotypes.png")
        self.log_artifact("phenotypes.png", "Ph_images")

        ret = trainer.test(self.swotted, test_data_loader)
        # ret is a (list of) dictionary that contains the logged values.
        
        # Log the both metrics
        self.log_metric("test_loss", ret[0]["test_loss"])

        return ret[0]["test_loss"]


@with_hydronaut(config_path='experiments_hydronaut/experiment_tuning.yaml')
def main(config):
    experiment=HyperparametersTuning(config)
    return experiment()

if __name__ == '__main__':
    sys.exit(main())
