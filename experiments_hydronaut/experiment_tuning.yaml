# PyTorch lightning handles all parallelization so there is no need to use a
# parallelized job launcher such as joblib here.
defaults:
  - hydronaut_config
  # Use the Optuna optimizer with the TPE sampler.
  - override hydra/sweeper: optuna
  - override hydra/sweeper/sampler: tpe
  - _self_

hydra:
  # Set the default mode to MULTIRUN to obviate passing --multirun on each
  # invocation.
  mode: MULTIRUN
  sweeper:
    # The objective function returns the loss so we seek to minimize it.
    direction: minimize
    # The study name is set to the experiment name but can be changed to
    # anything.
    study_name: ${experiment.name}
    # The number of Optuna optimization trials.
    n_trials: 30
    # The number of jobs. This only has an effect when a Hydra launcher that
    # supports concurrent jobs is used.
    n_jobs: 1
    sampler:
      # Initial seed for the sampler.
      seed: 123

    # Sweep parameters that will be used by the model.
    params:
      ++experiment.params.model.non_succession: 0.1, 0.25, 0.5, 1, 2
      ++experiment.params.model.sparsity: 0.5
      ++experiment.params.model.rank: 10
      ++experiment.params.model.twl: 3

experiment:
  name: 'SWoTTeD_tuning'
  description: SWoTTeD experiment which explores the hyper-parameters of the model to identify the best parameters
  exp_class: experiments_hydronaut.experiment:HyperparametersTuning
  params:
    data:
      filename: 'data/mimic_data_tuple.pkl'
      percent_train: 0.75
    training:
      batch_size: 30
      nepochs: 100
      lr: 1e-3
    model:
      rank: 10
      twl: 3
      metric: "Bernoulli"
      non_succession: 0.5
      sparsity: 0.5
    predict:
      nepochs: 100
      lr: 1e-3
    log:
      on_epoch: true
      sync_dist: true

  # Make modules under src importable.
  python:
    paths:
      - competitors/swotted/model
