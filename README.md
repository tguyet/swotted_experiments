
# SWoTTeD experiments repository

This repository is made to easily reproduce the experiments about the SWoTTeD model.

The three competitors are:
- CNTF
- LogPar
- SWIFT


### Installation

```bash
git clone --recurse-submodules git@gitlab.inria.fr:tguyet/swotted_experiments.git
```

Then, you will have to unzip the two files in the SWIFT repository.


Setup the python environment using the poetry configurator that creates a virtual environment with all the required Python libraries.
```bash
poetry install
```


### Run experiments

Enter the poetry virtual environment:
```bash
poetry shell
```

The experiments generic scripts are in the `experiments` repository. 
The results, wih the Rmd script to generate Figures are in subdirectories. Each subdirectory is dedicated to one experiment that has been run.


